plugins {
	id("net.freudasoft.gradle-cmake-plugin") version "0.0.4"
}

cmake {
	workingFolder.value(layout.projectDirectory.dir("cmake-build-debug"))
	sourceFolder.value(layout.projectDirectory)

	buildConfig.set("Debug")
	buildTarget.set("all")
}

//region CMake task utilities

fun cmakeRun(taskName: String, vararg commandLine: String, configuration: Exec.() -> Unit = {}) {
	task<Exec>(taskName) {
		commandLine(*commandLine)
		configuration()

		dependsOn(tasks["cmakeBuild"])
	}
}

fun cmakeBuild(taskName: String, target: String) {
	cmakeRun(taskName, "make", "-s", "-C", "cmake-build-debug", target)
}

//endregion

cmakeRun("run", "./server") {
	workingDir("cmake-build-debug/src")
	environment("reseau_logs" to "latest_only")

	standardInput = System.`in`
}

cmakeBuild("test", "test")

cmakeBuild("install", "install")

task<Delete>("clean") {
	delete("cmake-build-debug")
}
