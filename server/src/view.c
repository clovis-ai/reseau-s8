#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include "view.h"
#include "position.h"
#include "cli_command.h"
#include "command_reply.h"
#include "debug.h"
#include "connection.h"

struct view {
	const char *id;
	const struct absolute_position position;
};

struct view *view_create(const char *id, const struct absolute_position *position) {
	struct view *new = malloc(sizeof *new);
	new->id = id;
	position_absolute_copy((struct absolute_position *) &new->position, position);
	return new;
}

void view_free(struct view *view) {
	free((char *) view->id);
	free(view);
}

const char *view_id(const struct view *view) {
	return view->id;
}

const struct absolute_position *view_position(const struct view *view) {
	return &view->position;
}

void cli_add_view(int argc,
                  const char **argv,
                  struct aquarium *aquarium,
                  struct connection_data *connection __attribute__((unused))) {
	// Syntax: add view N1 500x500+400+200
	check(argc == 3, return, "Example: add view N1 500x500+400+200, found %d arguments.", argc);

	char *id = malloc(strlen(argv[1]));
	strcpy(id, argv[1]);

	struct absolute_position position;
	check(position_load(argv[2], &position) == 0, return, "Could not parse the position.");

	check(position_is_inside(&position, aquarium_position(aquarium)), return,
	      "Cannot add this view: it's outside the aquarium.");

	struct view *res = view_create(id, &position);
	int err = aquarium_add_view(aquarium, res);
	check(err == 0, return, "Could not add the view.");

	printf("\t-> view added\n");
}

void cli_del_view(int argc,
                  const char **argv,
                  struct aquarium *aquarium,
                  struct connection_data *connection __attribute__((unused))) {
	// Syntax: del view N5
	check(argc == 2, return, "Example: del view N5, found %d arguments", argc);

	struct view *view = aquarium_delete_view(aquarium, argv[1]);

	if (view == NULL) {
		printf("\t-> no view named %s\n", argv[1]);
	} else {
		printf("\t-> view %s deleted\n", argv[1]);
		delete_id(argv[1], aquarium);
		view_free(view);
	}
}
