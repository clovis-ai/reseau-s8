#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include "loader.h"
#include "server.h"
#include "debug.h"
#include "aquarium.h"
#include "command_parser.h"

#define PROMPT_BUFFER_SIZE 200

static void repl(struct aquarium *aquarium, const pthread_t *server) {
	// Without this fprintf, the program segfaults when ran with Gradle. Don't ask me why.
	fprintf(stdout, "Starting the REPL… %p\n", (void *) stdin);

	char input[PROMPT_BUFFER_SIZE] = {0};

	while (1) {
		printf("$ ");
		fflush(stdout);
		ssize_t nb_read = read(STDIN_FILENO, input, PROMPT_BUFFER_SIZE);
		check(nb_read != -1, exit(EXIT_FAILURE), "Couldn't read the standard input");
		input[nb_read] = 0;

		if (*input == 0) // Couldn't read
			break;

		const char quit_message[] = "quit"; // To stop the infinite loop
		if (memcmp(input, quit_message, strlen(quit_message)) == 0) {
			pthread_cancel(*server);
			break;
		}

		if (!parse_and_execute_cli(input, aquarium))
			printf("Could not parse the command. Try 'help'.\n");
	}
}

int main() {
	struct config config;
	struct server_config server_config;

	/**
	 * The aquarium the server is currently working on.
	 */
	config_load(&config);
	struct aquarium *aquarium = aquarium_create_empty(&config);
	log_message(INFO, "Loaded the following server configuration:\n"
	                  "\tcontroller-port: %ld\n"
	                  "\tdisplay-timeout-value: %ld\n"
	                  "\tfish-update-interval: %ld\n"
	                  "\tlog-location: %s",
	            config.controller_port, config.display_timeout_value, config.fish_update_interval,
	            config.log_location);
	server_config.config = config;
	server_config.aquarium = aquarium;

	start_subscriptions(aquarium);

	srand(time(NULL));
	pthread_t server_thread;
	pthread_create(&server_thread, NULL, (void *(*)(void *)) &start_server, &server_config);

	repl(aquarium, &server_thread);

	pthread_join(server_thread, NULL);
	stop_subscriptions(aquarium);
	aquarium_free(aquarium);
	return 0;
}
