#ifndef SERVER_CLI_COMMAND_H
#define SERVER_CLI_COMMAND_H

#include "command.h"
#include "aquarium.h"
#include "command_reply.h"

/**
 * Displays the list of supported CLI commands and their usage. Can provide a command name as argument
 * to target a specific command.
 * @param argc
 * @param argv
 */
void cli_help(int argc,
              const char **argv,
              struct aquarium *aquarium,
              struct connection_data *connection);

/**
 * Prints the aquarium's size and views. This command does not need any argument.
 * @param argc Should be 0 (any value will be ignored)
 * @param argv Should not contain any element (will be ignored)
 */
void cli_show_aquarium(int argc,
                       const char **argv,
                       struct aquarium *aquarium,
                       struct connection_data *connection);

/**
 * Saves the aquarium into the given file. The path must be accessible with write right.
 * @param argc Must be at least 1 for the filename
 * @param argv Must at least contain the filename
 */
void cli_save_aquarium(int argc,
                       const char **argv,
                       struct aquarium *aquarium,
                       struct connection_data *connection);

/**
 * Loads the aquarium from the given file. The path must be accessible with read right.
 * @param argc Must be at least 1 for the filename
 * @param argv Must at least contain the filename
 */
void cli_load_aquarium(int argc,
                       const char **argv,
                       struct aquarium *aquarium,
                       struct connection_data *connection);

// TODO: Document with instructions such as arguments to give
void cli_add_view(int argc,
                  const char **argv,
                  struct aquarium *aquarium,
                  struct connection_data *connection);

// TODO: Document with instructions such as arguments to give
void cli_del_view(int argc,
                  const char **argv,
                  struct aquarium *aquarium,
                  struct connection_data *connection);

#endif //SERVER_CLI_COMMAND_H
