#include <sys/queue.h>
#include <malloc.h>
#include <pthread.h>
#include "debug.h"
#include "fish.h"
#include "position.h"
#include "behavior.h"
#include "aquarium.h"

//region Waypoints

struct waypoint {
	const struct absolute_position position;
	time_t timestamp;
	SLIST_ENTRY(waypoint) entries;
};

//endregion

struct fish {
	const char *name;
	int started;
	struct absolute_position position;
	struct behavior *behavior;
	const struct aquarium *aquarium;
	pthread_mutex_t lock;
	SLIST_HEAD(waypoints, waypoint) waypoints;
};

struct fish *fish_create(const char *name,
                         const struct absolute_position *starting_position,
                         const struct aquarium *aquarium,
                         const char *behavior) {
	struct fish *new = malloc(sizeof *new);
	new->name = name;
	new->started = 0;
	new->aquarium = aquarium;
	position_absolute_copy(&new->position, starting_position);
	new->behavior = get_behavior(behavior);
	if (new->behavior == NULL) {
		warn("Mobility model is not valid");
		free(new);
		return NULL;
	}
	pthread_mutex_init(&new->lock, NULL);
	SLIST_INIT(&new->waypoints);

	add_waypoint(new, (struct absolute_position *) starting_position, time(NULL));

	return new;
}

void fish_free(struct fish *fish) {
	pthread_mutex_destroy(&fish->lock);
	free(fish->behavior);

	struct waypoint *wp;
	SLIST_FOREACH(wp, &fish->waypoints, entries) {
		free(wp);
	}

	free(fish);
}

//region Lock debug

static void lock_fish(struct fish *fish) {
	debug("%ld fish %s: request lock", get_thread(), fish->name);
	pthread_mutex_lock(&fish->lock);
	debug("%ld fish %s: lock owner", get_thread(), fish->name);
}

static void unlock_fish(struct fish *fish) {
	debug("%ld fish %s: release", get_thread(), fish->name);
	pthread_mutex_unlock(&fish->lock);
}

//endregion

const char *fish_name(const struct fish *fish) {
	// No need to lock because the name is immutable
	return fish->name;
}

const struct absolute_position *fish_position(const struct fish *fish) {

	lock_fish((struct fish *) fish);
	const struct waypoint *waypoint = SLIST_FIRST(&fish->waypoints);

	if (waypoint == NULL) {
		const struct absolute_position *position = &fish->position;
		unlock_fish((struct fish *) fish);
		return position;
	} else {
		const struct absolute_position *position = &waypoint->position;
		unlock_fish((struct fish *) fish);
		return position;
	}
}

void fish_start(struct fish *fish) {
	// No need to lock because this is atomic
	fish->started = 1;
}

int add_waypoint(struct fish *fish, struct absolute_position *position, time_t timestamp) {
	lock_fish(fish);
	int is_last = 0;
	struct waypoint *wp;
	if (SLIST_EMPTY(&fish->waypoints))
		wp = NULL;
	else {
		SLIST_FOREACH(wp, &fish->waypoints, entries) {
			if (wp->timestamp >= timestamp) {
				check_silent(wp->timestamp != timestamp, unlock_fish(fish); return -1,
				             "Cannot create a waypoint with timestamp %ld, because one already exists.",
				             timestamp);
				break;
			}
		}
		if (wp == NULL)
			is_last = 1;
	}

	struct waypoint *waypoint = malloc(sizeof *waypoint);
	check_silent(waypoint, unlock_fish(fish); return 1, "Could not allocate the waypoint");

	position_absolute_copy((struct absolute_position *) &waypoint->position, position);
	waypoint->timestamp = timestamp;

	debug("add_waypoint inserting");
	if (is_last == 1) {
		debug("add_waypoint insert in last position");
		SLIST_FOREACH(wp, &fish->waypoints, entries) {
			if (SLIST_NEXT(wp, entries) == NULL) {
				SLIST_INSERT_AFTER(wp, waypoint, entries);
				break;
			}
		}
	} else if (wp == NULL) {
		debug("add_waypoint insert at the start");
		SLIST_INSERT_HEAD(&fish->waypoints, waypoint, entries);
	} else {
		debug("add_waypoint insert in the middle");
		SLIST_INSERT_AFTER(wp, waypoint, entries);
	}
	unlock_fish(fish);

	return 0;
}

struct waypoint *next_waypoint(struct fish *fish) {
	struct waypoint *wp = malloc(sizeof *wp);

	next_waypoints(fish, wp, 1);
	return wp;
}

size_t next_waypoints(struct fish *fish, struct waypoint *output, size_t size) {
	check_silent(size > 0, return 0, "The given size should be positive: %zu", size);
	struct waypoint *wp;

	// Remove obsolete waypoints until reaching a valid one
	lock_fish(fish);
	while ((wp = SLIST_FIRST(&fish->waypoints)) != NULL && is_waypoint_obsolete(wp)) {
		SLIST_REMOVE_HEAD(&fish->waypoints, entries);
	}
	unlock_fish(fish);

	mobility_add_waypoints(fish, wp);

	lock_fish(fish);
	size_t i = 0;
	SLIST_FOREACH(wp, &fish->waypoints, entries) {
		if (i >= size)
			break;

		struct waypoint *res = output + i;
		position_absolute_copy((struct absolute_position *) &res->position, &wp->position);
		res->timestamp = wp->timestamp;

		++i;
	}
	unlock_fish(fish);

	return i;
}

const struct absolute_position *waypoint_position(struct waypoint *waypoint) {
	return &waypoint->position;
}

time_t waypoint_timestamp(struct waypoint *waypoint) {
	return waypoint->timestamp;
}

int is_waypoint_obsolete(struct waypoint *waypoint) {
	return waypoint != NULL && waypoint_remaining_time(waypoint) < 0;
}

struct behavior *fish_get_behavior(const struct fish *fish) {
	return fish->behavior;
}

const struct aquarium *fish_get_aquarium(const struct fish *fish) {
	return fish->aquarium;
}

long waypoint_remaining_time(struct waypoint *waypoint) {
	return (long) waypoint->timestamp - time(NULL);
}
