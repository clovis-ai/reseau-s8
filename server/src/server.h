#ifndef SERVER_SERVER_H
#define SERVER_SERVER_H

#include "loader.h"

#define MAXIMUM_WAITING_CONNECTIONS 10

struct server_config {
	struct config config;
	struct aquarium *aquarium;
};

struct connection_data;

_Noreturn void *start_server(const struct server_config *config);

/**
 * Closes all opened sockets and frees all the allocated variables
 * @param memory The structure containing everything to close and free. Must be of type struct server_memory.
 */
void server_clean(void *memory);

/**
 * Reset the time of last message for a connection
 * @param connection
 */
void connection_set_time(struct connection_data *connection);

#endif //SERVER_SERVER_H
