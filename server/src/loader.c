#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdbool.h>
#include "loader.h"
#include "debug.h"

#define BUFFER_SIZE 128
#define CONFIG_PATH "controller.cfg"
#define LOG_FILENAME_SIZE 25 // At most 19 for date and .txt and 1 for the end of string

/**
 * This structure stores a value and indicates the type of the currently stored value
 */
struct typed_value {
	bool is_initialized;
	enum CONFIG_VALUE_TYPE {
		LONG, STRING
	} type;

	union {
		long as_long;
		char *as_string;
	};
};

/**
 * This structure represents a configuration line and its value
 */
struct config_entry {
	const char *name;
	struct typed_value entry;
};

static unsigned int typed_value_empty(const struct typed_value *a) {
	return !a->is_initialized;
}

static bool typed_value_equals(const struct typed_value *a, const struct typed_value *b) {
	if (typed_value_empty(a) && typed_value_empty(b))
		return true;
	if (typed_value_empty(a) || typed_value_empty(b) || a->type != b->type)
		return false;

	switch (a->type) {
		case LONG:
			return a->as_long == b->as_long;
		case STRING:
			return strcmp(a->as_string, b->as_string) == 0;
		default:
			return false;
	}
}

//region dictionary
static struct config_entry dictionary[] = {
		{
				.name = "controller-port",
				.entry = {
						.is_initialized = false,
						.type = LONG,
						.as_long = -1
				}
		},
		{
				.name = "display-timeout-value",
				.entry = {
						.is_initialized = false,
						.type = LONG,
						.as_long = -1
				}
		},
		{
				.name = "fish-update-interval",
				.entry = {
						.is_initialized = false,
						.type = LONG,
						.as_long = -1
				}
		},
		{
				.name = "log-location",
				.entry = {
						.is_initialized = false,
						.type = STRING,
						.as_long = -1
				}
		}
};
//endregion

static const int dictionary_size = sizeof(dictionary) / sizeof(*dictionary);

#define err_file_not_found "The configuration was not found: %s"
#define err_not_recognized "The following server configuration file entry was not recognized: %s"
#define err_not_found "A value was not found for the following configuration setting: %s"
#define err_redundancy "Found two different entries for the following configuration setting: %s"

static int get_index(const char *buff) {
	for (int i = 0; i < dictionary_size; i++) {
		char next_char = buff[strlen(dictionary[i].name)];
		if ((buff == strstr(buff, dictionary[i].name)) &&
		    (next_char == ' ' || next_char == '='))
			return i;
	}
	return -1;
}

static char *get_value_from_line(char *buff) {
	for (; *buff != ' ' && *buff != '='; buff++);
	for (; *buff == ' ' || *buff == '='; buff++);
	return buff;
}

static void to_do_before_exit(FILE *file) {
	fclose(file);
	exit(EXIT_FAILURE);
}

static int log_fd;

void config_load(struct config *target) {
	log_message(INFO, "Loading configuration file...");
	FILE *file = fopen(CONFIG_PATH, "r");
	check(file != NULL, exit(EXIT_FAILURE), err_file_not_found, CONFIG_PATH);

	char buff[BUFFER_SIZE];
	char *res;

	while (1) {
		while ((res = fgets(buff, BUFFER_SIZE, file)) && (*buff == '#' || *buff == '\n'));
		if (res == NULL)
			break;

		int item_index;
		check((item_index = get_index(res)) != -1, to_do_before_exit(file), err_not_recognized,
		      res);

		struct typed_value item_value;
		item_value.as_string = get_value_from_line(res);

		// Remove trailing \n character
		item_value.as_string[strcspn(item_value.as_string, "\n")] = 0;

		// Set correct value type
		item_value.type = dictionary[item_index].entry.type;
		if (item_value.type == LONG)
			item_value.as_long = strtol(item_value.as_string, NULL, 10);

		item_value.is_initialized = true;

		// Check if another value was already set to given entry
		check(typed_value_empty(&(dictionary[item_index].entry)) ||
		      // Value already set
		      typed_value_equals(&(dictionary[item_index].entry),
		                         &item_value), // Set value is different
		      to_do_before_exit(file),
		      err_redundancy, dictionary[item_index].name);

		dictionary[item_index].entry = item_value;
	}

	for (int i = 0; i < dictionary_size; i++)
		check(!typed_value_empty(&(dictionary[i].entry)), to_do_before_exit(file),
		      err_not_found, dictionary[i].name);

	target->controller_port = dictionary[0].entry.as_long;
	target->display_timeout_value = dictionary[1].entry.as_long;
	target->fish_update_interval = dictionary[2].entry.as_long;
	strcpy(target->log_location, dictionary[3].entry.as_string);

	log_message(INFO, "Configuration file loaded...");
	fclose(file);

	char log_filename[LOG_FILENAME_SIZE];
	int call_res;
	size_t log_filename_size;
	if (getenv("reseau_logs") != NULL) {
		strcpy(log_filename, "latest.txt");
		log_filename_size = strlen(log_filename);
	} else {
		time_t t = time(NULL);
		struct tm tm = *localtime(&t);
		call_res = strftime(log_filename, LOG_FILENAME_SIZE, "%Y%m%d-%H%M%S.txt", &tm);
		check(call_res != 0, exit(EXIT_FAILURE), "Could not format time: Format too long");
		log_filename_size = LOG_FILENAME_SIZE;
	}

	char log_full_path[LOG_PATH_SIZE + log_filename_size + 1];
	strcpy(log_full_path, target->log_location);
	strcat(log_full_path, "/");
	strcat(log_full_path, log_filename);

	/* Try to create the log directory (or use it if exists) */
	call_res = mkdir(target->log_location, S_IRUSR | S_IWUSR | S_IXUSR);
	check(!(call_res == -1 && errno != EEXIST), exit(EXIT_FAILURE),
	      "Could not create the log directory: %s",
	      target->log_location);

	/* Try to create the log file */
	log_fd = open(log_full_path, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
	check(log_fd != -1, exit(EXIT_FAILURE), "Could not create the log file: %s", log_full_path);

	log_message(INFO, "All output now redirected to log file: %s", log_full_path);

	/* Redirect all writes to stderr to the log file */
	dup2(log_fd, STDERR_FILENO);
	close(log_fd);

	log_message(INFO, "First line of the logs after the redirection.");
}
