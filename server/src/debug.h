#ifndef SERVER_DEBUG_H
#define SERVER_DEBUG_H

#include <sys/syscall.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

#define format_string(buffer, text, ...) do { \
    size_t needed = snprintf(NULL, 0, text, __VA_ARGS__); \
    buffer = malloc(needed + 1); \
    sprintf(buffer, text, __VA_ARGS__); \
} while(0)

enum LOG_LEVEL {
	ERROR, WARN, INFO, DEBUG
};

#define log_message(level, message, ...) do { \
    switch (level) { \
    case ERROR: \
        fprintf(stderr, "[ERROR]\t%s:%d "message"\n", __FILE__, __LINE__, ##__VA_ARGS__); \
        break; \
    case WARN: \
        fprintf(stderr, "[WARN]\t%s:%d "message"\n", __FILE__, __LINE__, ##__VA_ARGS__); \
        break; \
    case INFO: \
        fprintf(stderr, "[INFO]\t%s:%d "message"\n", __FILE__, __LINE__, ##__VA_ARGS__); \
        break; \
    case DEBUG: \
        fprintf(stderr, "[DEBUG]\t%s:%d "message"\n", __FILE__, __LINE__, ##__VA_ARGS__); \
        break; \
    default: \
        fprintf(stderr, "%s:%d "message"\n",  __FILE__, __LINE__, ##__VA_ARGS__); \
        break; \
    }                                              \
} while(0)

#define error(message, ...) log_message(ERROR, message, ##__VA_ARGS__)
#define warn(message, ...) log_message(WARN, message, ##__VA_ARGS__)
#define info(message, ...) log_message(INFO, message, ##__VA_ARGS__)
#define debug(message, ...) log_message(DEBUG, message, ##__VA_ARGS__)

#define check_message(message, ...) do { \
    printf("\t-> Failed: " message "\n", ##__VA_ARGS__); \
} while(0)

#define check_internal(silent, condition, action, ...) do { \
    if (!(condition)) { \
        if (errno != 0) { \
            log_message(ERROR, ##__VA_ARGS__); \
            perror("Reason");                   \
        } else { \
            log_message(ERROR, ##__VA_ARGS__); \
        }        \
        if (!silent)\
            check_message(__VA_ARGS__); \
        action; \
    } \
    errno = 0; \
} while(0)

/**
 * If the given condition is true, does nothing.
 * If it is false, performs 'action' and prints the message given as
 * last argument (printf format) to the logs (but not the standard output).
 */
#define check_silent(condition, action, ...) check_internal(1, condition, action, ##__VA_ARGS__)

/**
 * If the given condition is true, does nothing.
 * If it is false, performs 'action' and prints the message given as
 * last argument (printf format) to the logs and the standard output.
 */
#define check(condition, action, ...) check_internal(0, condition, action, ##__VA_ARGS__)

#define get_thread() syscall(__NR_gettid)

#endif //SERVER_DEBUG_H
