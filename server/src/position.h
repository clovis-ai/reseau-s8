#ifndef SERVER_POSITION_H
#define SERVER_POSITION_H

#include <stddef.h>

/**
 * An absolute position, as used by the server to store data.
 */
struct absolute_position {
	/** The coordinate on the horizontal axis (from left 0 to right). */
	unsigned int x;
	/** The coordinate on the vertical axis (from top 0 to bottom). */
	unsigned int y;

	/** The width of this position (such that right side == x + width). The width should not be 0. */
	unsigned int width;
	/** The height of this position (such that bottom side == y + height). The height should not be 0. */
	unsigned int height;
};

/**
 * A relative position, as used to communicate with each client.
 * Because this is relative, it is not possible to compare two client_position.
 */
struct client_position {
	/** The coordinate on the horizontal axis (from left 0 to right, relative to the screen size). */
	int x;
	/** The coordinate on the vertical axis (from top 0 to bottom, relative to the screen size). */
	int y;

	/** The width of this position, in percent of the height of the screen (0..100) */
	unsigned int widthPercent;
	/** The height of this position, in percent of the height of the screen (0..100) */
	unsigned int heightPercent;
};

/**
 * Converts an absolute position to a relative position in a frame of reference.
 * @param position The absolute position you want to convert
 * @param client The frame of reference
 * @param output The output relative position (caller's responsibility to allocate)
 */
void position_to_relative(const struct absolute_position *position,
                          const struct absolute_position *client,
                          struct client_position *output);

/**
 * Converts a relative position in a frame of reference to an absolute position.
 * @param position The relative position you want to convert
 * @param client The frame of reference
 * @param output The output absolute position (caller's responsibility to allocate)
 */
void position_to_absolute(const struct client_position *position,
                          const struct absolute_position *client,
                          struct absolute_position *output);

/**
 * Checks to see if a position is inside another.
 * @param position The position you want to check
 * @param frame The frame inside which the position should be
 * @return 1 if the position is entirely inside the frame, 0 if it isn't
 */
int position_is_inside(const struct absolute_position *position,
                       const struct absolute_position *frame);

/**
 * Copies an absolute position.
 * @param destination The target of the copy (will be overwritten).
 * @param source The source of the copy (untouched).
 */
void position_absolute_copy(struct absolute_position *destination,
                            const struct absolute_position *source);

/**
 * Copies a relative position.
 * @param destination The target of the copy (will be overwritten).
 * @param source The source of the copy (untouched).
 */
void position_client_copy(struct client_position *destination,
                          const struct client_position *source);

/**
 * Translates a position by a vector.
 *
 * The width and height of the base position are conserved.
 * @param base The position that will be translated
 * @param translation The translation direction
 * @param output The resulting position.
 */
void position_absolute_translate(const struct absolute_position *base,
                                 const struct absolute_position *translation,
                                 struct absolute_position *output);

/**
 * Loads a position from a string
 * @param text A position represented as "400x400+400+400"
 * @param output The position that will be edited
 * @return 0 if everything went well, an error code otherwise.
 */
int position_load(const char *text, struct absolute_position *output);

/**
 * Saves a position into a string
 * @param input The position that needs to be saved
 * @param output_buffer The buffer in which the position will be written (should be pre-allocated with the correct size)
 * @param output_buffer_size The size of the buffer
 * @return 0 if everything went well, an error code otherwise.
 */
int position_save(const struct absolute_position *input, char *output_buffer, size_t output_buffer_size);

#endif //SERVER_POSITION_H
