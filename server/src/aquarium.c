#include <pthread.h>
#include <sys/queue.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <stdio.h>
#include "debug.h"
#include "aquarium.h"
#include "fish.h"
#include "command.h"
#include "connection.h"

//region Lists

struct fish_node {
	struct fish *value;
	SLIST_ENTRY(fish_node) entries;
};
SLIST_HEAD(fish_list, fish_node);

struct view_node {
	struct view *value;
	SLIST_ENTRY(view_node) entries;
};
SLIST_HEAD(view_list, view_node);

struct update_request {
	struct connection_data *connection;
	struct view *view;
	SLIST_ENTRY(update_request) entries;
};
SLIST_HEAD(update_request_list, update_request);

//endregion

struct aquarium {
	struct fish_list fishes;
	struct view_list views;
	struct absolute_position position;

	pthread_mutex_t fish_lock;
	pthread_mutex_t view_lock;
	pthread_mutex_t request_lock;

	struct update_request_list update_requests;
	pthread_t update_request_thread;
	int update_requests_started;

	struct config *config;
};

struct aquarium *aquarium_create_empty(struct config *config) {
	struct aquarium *new = malloc(sizeof *new);
	SLIST_INIT(&new->fishes);
	SLIST_INIT(&new->views);
	SLIST_INIT(&new->update_requests);

	struct absolute_position *position = (struct absolute_position *) &new->position;
	position->x = 0;
	position->y = 0;
	position->width = 1000;
	position->height = 1000;

	pthread_mutex_init(&new->fish_lock, NULL);
	pthread_mutex_init(&new->view_lock, NULL);
	pthread_mutex_init(&new->request_lock, NULL);

	new->update_requests_started = 0;
	new->config = config;

	return new;
}

void aquarium_free(struct aquarium *aquarium) {
	aquarium_reset(aquarium);

	pthread_mutex_destroy(&aquarium->fish_lock);
	pthread_mutex_destroy(&aquarium->view_lock);
	pthread_mutex_destroy(&aquarium->request_lock);

	free(aquarium);
}

//region Locks debug

static void lock_fishes(struct aquarium *aquarium) {
	debug("%ld fishes: request lock", get_thread());
	pthread_mutex_lock(&aquarium->fish_lock);
	debug("%ld fishes: lock owner", get_thread());
}

static void unlock_fishes(struct aquarium *aquarium) {
	debug("%ld fishes: release", get_thread());
	pthread_mutex_unlock(&aquarium->fish_lock);
}

static void lock_views(struct aquarium *aquarium) {
	debug("%ld views: request lock", get_thread());
	pthread_mutex_lock(&aquarium->fish_lock);
	debug("%ld views: lock owner", get_thread());
}

static void unlock_views(struct aquarium *aquarium) {
	debug("%ld views: release", get_thread());
	pthread_mutex_unlock(&aquarium->fish_lock);
}

static void lock_requests(struct aquarium *aquarium) {
	debug("%ld requests: request lock", get_thread());
	pthread_mutex_lock(&aquarium->request_lock);
	debug("%ld requests: lock owner", get_thread());
}

static void unlock_requests(struct aquarium *aquarium) {
	debug("%ld requests: release", get_thread());
	pthread_mutex_unlock(&aquarium->request_lock);
}

//endregion
//region Load & save

int aquarium_load(struct aquarium *aquarium, const char *filename) {

	aquarium_reset(aquarium);
	FILE *file = fopen(filename, "r");
	check(file, return -1, "Couldn't open file %s", filename);
	log_message(DEBUG, "Opened configuration file %s", filename);

	enum BUFFER_SIZE {
		BUFFER_SIZE = 50
	};
	char buffer[BUFFER_SIZE];
	fgets(buffer, BUFFER_SIZE, file);

	// First line: 1000x1000 (WIDTHxHEIGHT)
	char *buffer_position = buffer;
	long width = strtol(buffer_position, &buffer_position, 10);
	long height = strtol(buffer_position + 1, &buffer_position, 10);
	check(width > 0, return -1, "The width should be strictly positive: found %ld", width);
	check(height > 0, return -1, "The height should be strictly positive: found %ld",
	      height);

	log_message(DEBUG, "Found an aquarium of size %ld×%ld", width, height);
	aquarium->position.width = width;
	aquarium->position.height = height;

	while (fgets(buffer, BUFFER_SIZE, file)) {
		// Each line: N1 0x0+500+500 (ID XxY+WIDTH+HEIGHT)

		// Find the first space
		int first_space = 0;
		for (; buffer[first_space] && buffer[first_space] != ' '; first_space++);
		check(buffer[first_space], return -1,
		      "The format is 'N1 0x0+500+500', found no space character.");
		buffer[first_space] = 0;

		// Get the ID
		char *id = malloc(first_space * sizeof(char));
		strcpy(id, buffer);

		// Get the coordinates
		long view_x = strtol(buffer + first_space + 1, &buffer_position, 10);
		long view_y = strtol(buffer_position + 1, &buffer_position, 10);
		long view_width = strtol(buffer_position + 1, &buffer_position, 10);
		long view_height = strtol(buffer_position + 1, &buffer_position, 10);
		check(*buffer_position == '\n', return -1,
		      "Expected the line to end after the fourth integer, but it hasn't: '%s'",
		      buffer_position);
		check(view_x > 0, return -1,
		      "The x position should be strictly positive: found %ld", view_x);
		check(view_y > 0, return -1,
		      "The y position should be strictly positive: found %ld", view_y);
		check(view_width > 0, return -1,
		      "The width should be strictly positive: found %ld", view_width);
		check(view_height > 0, return -1,
		      "The x position should be strictly positive: found %ld", view_height);

		struct absolute_position position = {
				.x = view_x,
				.y = view_y,
				.width = view_width,
				.height = view_height,
		};
		struct view *view = view_create(id, &position);
		aquarium_add_view(aquarium, view);
	}

	fclose(file);
	return 0;
}

int aquarium_save(const struct aquarium *aquarium, const char *filename) {
	log_message(DEBUG, "Saving the aquarium to %s", filename);

	FILE *file = fopen(filename, "w");
	check(file, return -1, "Couldn't open file %s", filename);

	fprintf(file, "%ux%u\n", aquarium->position.width, aquarium->position.height);

	struct view_node *view;
	SLIST_FOREACH(view, &aquarium->views, entries) {
		const struct absolute_position *position = view_position(view->value);
		fprintf(file, "%s %dx%d+%d+%d\n", view_id(view->value),
		        position->x, position->y, position->width, position->height);
	}

	fclose(file);
	log_message(INFO, "Aquarium saved.");
	return 0;
}

int aquarium_reset(struct aquarium *aquarium) {
	lock_views(aquarium);
	while (!SLIST_EMPTY(&aquarium->views)) {
		struct view_node *view = SLIST_FIRST(&aquarium->views);
		check(delete_id(view_id(view->value), aquarium) == 0, return -1, "Could not delete view");
		SLIST_REMOVE_HEAD(&aquarium->views, entries);
		view_free(view->value);
		free(view);
	}
	unlock_views(aquarium);

	lock_fishes(aquarium);
	while (!SLIST_EMPTY(&aquarium->fishes)) {
		struct fish_node *fish = SLIST_FIRST(&aquarium->fishes);
		SLIST_REMOVE_HEAD(&aquarium->fishes, entries);
		fish_free(fish->value);
		free(fish);
	}
	unlock_fishes(aquarium);

	lock_requests(aquarium);
	while (!SLIST_EMPTY(&aquarium->update_requests)) {
		struct update_request *request = SLIST_FIRST(&aquarium->update_requests);
		SLIST_REMOVE_HEAD(&aquarium->update_requests, entries);
		free(request);
	}
	unlock_requests(aquarium);

	return 0;
}

//endregion
//region Fish

int aquarium_add_fish(struct aquarium *aquarium, struct fish *fish) {
	struct fish_node *node = malloc(sizeof *node);
	node->value = fish;

	lock_fishes(aquarium);
	SLIST_INSERT_HEAD(&aquarium->fishes, node, entries);
	unlock_fishes(aquarium);

	return 0;
}

struct fish *aquarium_delete_fish(struct aquarium *aquarium, const char *fish) {
	struct fish_node *node;

	lock_fishes(aquarium);
	SLIST_FOREACH(node, &aquarium->fishes, entries) {
		if (strcmp(fish_name(node->value), fish) == 0) {
			SLIST_REMOVE(&aquarium->fishes, node, fish_node, entries);
			unlock_fishes(aquarium);
			return node->value;
		}
	}
	unlock_fishes(aquarium);

	return NULL;
}

//endregion
//region Views

int aquarium_add_view(struct aquarium *aquarium, struct view *view) {
	struct view_node *v;
	SLIST_FOREACH(v, &aquarium->views, entries) {
		check(strcmp(view_id(v->value), view_id(view)) != 0, return -1,
		      "A view with the name '%s' already exists.", view_id(v->value));
	}

	log_message(INFO, "Adding view %s (%d×%d + %d×%d)", view_id(view), view_position(view)->x,
	            view_position(view)->y, view_position(view)->width, view_position(view)->height);

	struct view_node *node = malloc(sizeof *node);
	node->value = view;

	lock_views(aquarium);
	SLIST_INSERT_HEAD(&aquarium->views, node, entries);
	unlock_views(aquarium);

	add_id(view_id(view), aquarium);

	return 0;
}

struct view *aquarium_delete_view(struct aquarium *aquarium, const char *view) {
	struct view_node *node;

	lock_views(aquarium);
	SLIST_FOREACH(node, &aquarium->views, entries) {
		if (strcmp(view_id(node->value), view) == 0) {
			SLIST_REMOVE(&aquarium->views, node, view_node, entries);
			unlock_views(aquarium);
			delete_id(view, aquarium);
			return node->value;
		}
	}
	unlock_views(aquarium);

	return NULL;
}

const struct absolute_position *aquarium_position(const struct aquarium *aquarium) {
	return &aquarium->position;
}

static struct view *get_view_from_connection(struct aquarium *aquarium,
                                             struct connection_data *connection) {
	// Getting the session ID from the connection
	struct session *session = get_session_from_connection(connection);
	char session_id[20];
	session_get_id(session, session_id);

	// Getting the client view from the session ID
	struct view_node *view_node;
	SLIST_FOREACH(view_node, &aquarium->views, entries) {
		if (strcmp(session_id, view_id(view_node->value)) == 0)
			return view_node->value;
	}
	network_nok_reply("View not found, the client is probably not authenticated.\n", connection);
	return NULL;
}

//endregion
//region CLI commands

void cli_show_aquarium(int argc __attribute__((unused)),
                       const char **argv __attribute__((unused)),
                       struct aquarium *aquarium,
                       struct connection_data *connection __attribute__((unused))) {

	printf("%dx%d\n", aquarium->position.width, aquarium->position.height);
	struct view_node *view;
	SLIST_FOREACH(view, &aquarium->views, entries) {
		const struct absolute_position *position = view_position(view->value);
		printf("%s %dx%d+%d+%d\n", view_id(view->value), position->x, position->y, position->width,
		       position->height);
	}
}

void cli_save_aquarium(int argc,
                       const char **argv,
                       struct aquarium *aquarium,
                       struct connection_data *connection __attribute__((unused))) {

	check(argc == 2, return, "Wrong number of argument, 1 expected");
	check(aquarium_save(aquarium, argv[1]) == 0,
	      return, "Server could not save the aquarium");

	int nb_view = 0;
	struct view_node *node;

	lock_views(aquarium);
	SLIST_FOREACH(node, &aquarium->views, entries) {
		nb_view++;
	}
	unlock_views(aquarium);
	cli_reply("Aquarium saved (%d display views)!\n", nb_view);
}

void cli_load_aquarium(int argc,
                       const char **argv,
                       struct aquarium *aquarium,
                       struct connection_data *connection __attribute__((unused))) {

	check(argc == 2, return, "Wrong number of argument, 1 expected");
	check(aquarium_load(aquarium, argv[1]) == 0, return, "Server could not load the aquarium");

	int nb_view = 0;
	struct view_node *node;

	lock_views(aquarium);
	SLIST_FOREACH(node, &aquarium->views, entries) {
		nb_view++;
	}
	unlock_views(aquarium);
	cli_reply("Aquarium loaded (%d display views)!\n", nb_view);
}

//endregion
//region Network commands

int str_to_position(char *str, struct client_position *client_position) {
	char *buffer_position = str;
	char *temp_buffer_position;
	long pos[4] = {0};
	for (int i = 0; i < 4; ++i) {
		pos[i] = strtol(buffer_position, &temp_buffer_position, 10);
		if (buffer_position == temp_buffer_position) {
			return -1;
		}
		if ((pos[i] < 0) || (i % 2 == 0 && *temp_buffer_position != 'x') ||
		    (i % 2 == 1 && *temp_buffer_position != ',')) {
			return -1;
		}
		buffer_position = temp_buffer_position + 1;
	}
	client_position->x = (int) pos[0];
	client_position->y = (int) pos[1];
	client_position->widthPercent = (unsigned int) pos[2];
	client_position->heightPercent = (unsigned int) pos[3];
	return 0;
}

void network_add_fish(int argc,
                      const char **argv,
                      struct aquarium *aquarium,
                      struct connection_data *connection) {
	check(argc == 5, {
		network_nok_reply(
				"Expected 5 arguments",
				connection);
		return;
	}, "Received wrong message from client: number of arguments");
	check(strcmp(argv[2], "at") == 0, {
		network_nok_reply(
				"Wrong syntax, expected \"at\"",
				connection);
		return;
	}, "Received wrong message from client: wrong syntax");

	char pos[strlen(argv[3])];
	strcpy(pos, argv[3]);
	struct client_position starting_position;

	check(str_to_position(pos, &starting_position) == 0, {
		network_nok_reply(
				"Wrong position syntax, should be : [x]x[y],[width]x[height],\n",
				connection);
		return;
	}, "Received wrong message from client: wrong position syntax");

	struct absolute_position absolute_starting_position;
	struct view *view = get_view_from_connection(aquarium, connection);
	position_to_absolute(&starting_position, view_position(view),
	                     &absolute_starting_position);

	char *name = malloc(sizeof(argv[1]));
	strcpy(name, argv[1]);
	struct fish_node *node;
	SLIST_FOREACH(node, &aquarium->fishes, entries) {
		if (strcmp(fish_name(node->value), name) == 0) {
			network_nok_reply("The fish name already exists\n", connection);
			return;
		}
	}
	struct fish *fish = fish_create(name, &absolute_starting_position,
	                                aquarium, argv[4]);
	if (fish == NULL) {
		warn("Could not create fish");
		network_nok_reply("Could not create fish. Is mobility model correct ?\n", connection);
		return;
	}
	aquarium_add_fish(aquarium, fish);
	network_ok_reply("\n", connection);
}

void network_del_fish(int argc,
                      const char **argv,
                      struct aquarium *aquarium,
                      struct connection_data *connection) {
	if (!(argc == 2 && strcmp(argv[0], "delFish") == 0)) {
		network_nok_reply("Wrong syntax, should be delFish [fishName]\n", connection);
	} else if (aquarium_delete_fish(aquarium, argv[1]) == NULL) {
		network_nok_reply("the fish does not exist\n", connection);
	} else {
		network_ok_reply("\n", connection);
	}
}

void network_start_fish(int argc, const char **argv, struct aquarium *aquarium,
                        struct connection_data *connection) {
	if (argc != 2) {
		network_nok_reply("Incorrect number of arguments\n", connection);
	}
	struct fish_node *fish_node;
	SLIST_FOREACH(fish_node, &aquarium->fishes, entries) {
		if (strcmp(fish_name(fish_node->value), argv[1]) == 0) {
			fish_start(fish_node->value);
			network_ok_reply("\n", connection);
			return;
		}
	}
	char *buffer;
	format_string(buffer, "Fish not recognized %s: \n", argv[1]);
	network_nok_reply(buffer, connection);
	free(buffer);
}

void get_fishes(struct aquarium *aquarium, struct view *view, struct connection_data *connection) {
	char message[1024] = "list";
	char *buffer;

	struct fish_node *fish;
	SLIST_FOREACH(fish, &aquarium->fishes, entries) {

		const struct absolute_position *fish_pos_absolute = fish_position(fish->value);
		const struct absolute_position *view_pos_absolute = view_position(view);

		if (position_is_inside(fish_pos_absolute, view_pos_absolute)) {

			struct client_position fish_pos;
			position_to_relative(fish_pos_absolute, view_pos_absolute, &fish_pos);

			struct waypoint *waypoint = next_waypoint(fish->value);

			format_string(buffer, " [%s at %dx%d,%dx%d,%ld]",
			              fish_name(fish->value),
			              fish_pos.x, fish_pos.y,
			              fish_pos.widthPercent, fish_pos.heightPercent,
			              (long) waypoint_timestamp(waypoint) - time(NULL));
			strcat(message, buffer);
			free(buffer);
		}

	}

	size_t message_length = strlen(message);
	message[message_length] = '\n';
	message[message_length + 1] = 0;
	network_reply(message, connection);
}

void network_get_fishes(int argc __attribute__((unused)),
                        const char **argv __attribute__((unused)),
                        struct aquarium *aquarium,
                        struct connection_data *connection) {
	lock_fishes(aquarium);

	get_fishes(aquarium, get_view_from_connection(aquarium, connection), connection);

	unlock_fishes(aquarium);
}

void network_get_fishes_continuously(int argc __attribute__((unused)),
                                     const char **argv __attribute__((unused)),
                                     struct aquarium *aquarium,
                                     struct connection_data *connection) {
	struct view *view = get_view_from_connection(aquarium, connection);
	check_silent(view != NULL, return,
	             "Could not get the view associated to this client, perhaps they disconnected during the command.");
	info("Client %s is subscribing to updates", view_id(view));

	lock_requests(aquarium);

	struct update_request *request = malloc(sizeof *request);
	request->view = view;
	request->connection = connection;
	SLIST_INSERT_HEAD(&aquarium->update_requests, request, entries);

	unlock_requests(aquarium);
}

//endregion
//region Subscription to updates

static void *handle_subscriptions(struct aquarium *aquarium) {
	debug("Started update subscriptions");
	while (aquarium->update_requests_started) {
		lock_requests(aquarium);
		lock_fishes(aquarium);

		struct update_request *request;
		SLIST_FOREACH(request, &aquarium->update_requests, entries) {

			info("Sending updates to %s…", view_id(request->view));
			get_fishes(aquarium, request->view, request->connection);
		}

		unlock_fishes(aquarium);
		unlock_requests(aquarium);

		sleep(aquarium->config->fish_update_interval);
	}
	debug("Stopped update subscriptions");
	return NULL;
}

void start_subscriptions(struct aquarium *aquarium) {
	lock_requests(aquarium);

	if (!aquarium->update_requests_started) {
		aquarium->update_requests_started = 1;
		debug("Starting update subscriptions");
		pthread_create(&aquarium->update_request_thread, NULL,
		               (void *(*)(void *)) handle_subscriptions, aquarium);
	} else
		warn("The update subscription system has already been started");

	unlock_requests(aquarium);
}

void stop_subscriptions(struct aquarium *aquarium) {
	debug("Registering the request to stop the subscription system");

	// Atomic operation, no need to lock.
	aquarium->update_requests_started = 0;

	pthread_join(aquarium->update_request_thread, NULL);
}

//endregion
