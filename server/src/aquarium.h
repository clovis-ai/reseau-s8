#ifndef SERVER_AQUARIUM_H
#define SERVER_AQUARIUM_H

#include "position.h"
#include "view.h"
#include "loader.h"

struct aquarium;

#include "fish.h"

/**
 * Heap-allocates an empty aquarium.
 * @return An empty aquarium.
 */
struct aquarium *aquarium_create_empty(struct config *config);

/**
 * Deeply heap-deallocates an aquarium.
 * @param aquarium An aquarium
 */
void aquarium_free(struct aquarium *aquarium);

/**
 * Gets the aquarium's position
 * @param aquarium The aquarium in question
 * @return The original aquarium's position (do not free)
 */
const struct absolute_position *aquarium_position(const struct aquarium *aquarium);

//region Load & save

/**
 * Heap-allocates an aquarium stored on disk.
 * @param filename The file to read from
 * @return An aquarium.
 */
int aquarium_load(struct aquarium *aquarium, const char *filename);

/**
 * Saves an aquarium to disk.
 * This function does NOT deallocate the aquarium. See aquarium_free.
 * @param filename The file to write to
 * @return 0 if everything went well.
 */
int aquarium_save(const struct aquarium *aquarium, const char *filename);

/**
 * Resets an aquarium without freeing it.
 * @param aquarium the aquarium to reset
 * @return 0 if everything went well.
 */
int aquarium_reset(struct aquarium *aquarium);

//endregion
//region Fish

/**
 * Adds a fish to the aquarium.
 * @param aquarium The aquarium in question
 * @param fish The fish that should join
 * @return 0 if everything went well
 */
int aquarium_add_fish(struct aquarium *aquarium, struct fish *fish);

/**
 * Removes a fish from the aquarium.
 * @param aquarium The aquarium in question
 * @param fish The name of the fish that should be removed
 * @return The fish that was removed, or NULL if it couldn't be found.
 */
struct fish *aquarium_delete_fish(struct aquarium *aquarium, const char *fish);

//endregion
//region Views

/**
 * Adds a view to the aquarium.
 * @param aquarium The aquarium in question
 * @param view The view that should be added
 * @return 0 if everything went well
 */
int aquarium_add_view(struct aquarium *aquarium, struct view *view);

/**
 * Removes a view from the aquarium
 * @param aquarium The aquarium in question
 * @param view The ID of the view that should be removed
 * @return The view that was removed, or NULL if it couldn't be found.
 */
struct view *aquarium_delete_view(struct aquarium *aquarium, const char *view);

//endregion
//region Subscription to updates

/**
 * Starts the subscription system.
 *
 * When the system is started, clients will receive periodic 'list' commands, after they subscribed with 'getFishesContinuously'.
 * The system can be started and stopped as many times as necessary.
 *
 * @param aquarium The aquarium for which the system should be started
 */
void start_subscriptions(struct aquarium *aquarium);

/**
 * Stops the subscription system (blocks until it is stopped).
 *
 * @param aquarium The aquarium for which the system should be stopped
 */
void stop_subscriptions(struct aquarium *aquarium);

//endregion

#endif //SERVER_AQUARIUM_H
