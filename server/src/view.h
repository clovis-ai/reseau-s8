#ifndef SERVER_VIEW_H
#define SERVER_VIEW_H

#include "position.h"

struct view;

/**
 * Heap-allocates a new view.
 * @param id The view's ID, which will be freed by view_free
 * @param position The view's position (will be copied)
 * @return A new view.
 * @see view_free
 */
struct view *view_create(const char *id, const struct absolute_position *position);

/**
 * Heap-deallocates a new view.
 * @param view The view in question
 * @see view_create
 */
void view_free(struct view *view);

/**
 * Gets the view's ID.
 * @param view The view in question
 * @return The view's ID
 */
const char *view_id(const struct view *view);

/**
 * Gets the view's position.
 * @param view The view in question
 * @return The view's position.
 */
const struct absolute_position *view_position(const struct view *view);

#endif //SERVER_VIEW_H
