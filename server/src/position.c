#include <string.h>
#include <stdlib.h>
#include "position.h"
#include "debug.h"

void position_to_relative(const struct absolute_position *position,
                          const struct absolute_position *client,
                          struct client_position *output) {
	output->x = position->x - client->x; // NOLINT(cppcoreguidelines-narrowing-conversions)
	output->y = position->y - client->y; // NOLINT(cppcoreguidelines-narrowing-conversions)
	output->widthPercent = position->width * 100 / client->width;
	output->heightPercent = position->height * 100 / client->height;
}

void position_to_absolute(const struct client_position *position,
                          const struct absolute_position *client,
                          struct absolute_position *output) {
	output->x = position->x + client->x;
	output->y = position->y + client->y;
	output->width = position->widthPercent * client->width / 100;
	output->height = position->heightPercent * client->height / 100;
}

int position_is_inside(const struct absolute_position *position,
                       const struct absolute_position *frame) {
	return position->x >= frame->x &&
	       position->y >= frame->y &&
	       position->x + position->height <= frame->x + frame->height &&
	       position->y + position->width <= frame->y + frame->width;
}

void position_absolute_copy(struct absolute_position *destination,
                            const struct absolute_position *source) {
	memcpy(destination, source, sizeof *destination);
}

void position_client_copy(struct client_position *destination,
                          const struct client_position *source) {
	memcpy(destination, source, sizeof *destination);
}

void position_absolute_translate(const struct absolute_position *base,
                                 const struct absolute_position *translation,
                                 struct absolute_position *output) {
	output->x = base->x + translation->x;
	output->y = base->y + translation->y;
	output->width = base->width;
	output->height = base->height;
}

int position_load(const char *text, struct absolute_position *output) {
	char *pos = (char *) text; // The original text will not be modified

	output->x = strtol(pos, &pos, 10);
	output->y = strtol(pos+1, &pos, 10);
	output->width = strtol(pos+1, &pos, 10);
	output->height = strtol(pos+1, &pos, 10);

	check(output->width > 0, return 1, "The position's width should be strictly positive, found %d", output->width);
	check(output->height > 0, return 1, "The position's height should be strictly positive, found %d", output->height);

	return 0;
}

int position_save(const struct absolute_position *input,
                  char *output_buffer,
                  size_t output_buffer_size) {
	int res = snprintf(output_buffer, output_buffer_size, "%dx%d+%d+%d", input->x, input->y, input->width, input->height);

	return res < 0 ? -res :
		res * 1ul > output_buffer_size ? 1 :
		0;
}
