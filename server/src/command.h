#ifndef SERVER_COMMAND_H
#define SERVER_COMMAND_H

#include "aquarium.h"
#include "command_reply.h"

struct command {
	/**
	 * The name of the command, can be composed of multiple words separated by a space.
	 */
	const char *name;

	/**
	 * The function pointer related to the command, which must be called when the command is
	 * launched.
	 * @param argc
	 * @param argv
	 * @param aquarium The aquarium to be interacted with
	 * @param connection The current connection to the client
	 */
	void (*handler)(int argc,
	                const char **argv,
	                struct aquarium *aquarium,
	                struct connection_data *connection);

	/**
	 * A string explaining how to use the command.
	 */
	const char *usage;

	/**
	 * A string describing the use of the command.
	 */
	const char *description;
};

#endif //SERVER_COMMAND_H
