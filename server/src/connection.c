#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/queue.h>
#include "connection.h"
#include "aquarium.h"
#include "debug.h"
#include "command_reply.h"

struct session {
	int id; // Matches to the corresponding View id
	char type;
	const struct connection_data *connection;
	const struct aquarium *aquarium;
	int used;
};

struct sessions_list {
	struct session *session;
	LIST_ENTRY(sessions_list) entries;
};

enum request_type {
	HELLO,
	HELLO_AS,
	INVALID
};

LIST_HEAD(listhead, sessions_list) SESSIONS;

char *types = "N";
char *command = "hello in as ";

void __attribute__((constructor)) authenticator_create();

void __attribute__((destructor)) authenticator_free();

void authenticator_create() {
	LIST_INIT(&SESSIONS);
}

void authenticator_free() {
	struct sessions_list *current;
	LIST_FOREACH(current, &SESSIONS, entries) {
		free(current->session);
		LIST_REMOVE(current, entries);
	}
}

int exists_id(char type, int id, const struct aquarium *aquarium) {
	struct sessions_list *current;
	LIST_FOREACH(current, &SESSIONS, entries) {
		if (current->session->type == type &&
		    current->session->id == id &&
		    current->session->aquarium == aquarium)
			return 1;
	}
	return 0;
}

int is_valid_id(const char *id) {
	char *end_num;
	strtol(id + 1, &end_num, 10);
	return (strpbrk(id, types) == id) && (*end_num == 0);
}

enum request_type get_request_type(const char *request) {
	if (strcmp(request, "hello") == 0)
		return HELLO;
	else {
		size_t length = strlen(command);
		check(strncmp(request, command, length) == 0, return INVALID, "Expected request to start with \"hello in as\"");
		check(is_valid_id(request + length), return INVALID, "Trying to register invalid ID");
		return HELLO_AS;
	}
}

const char *extract_id(const char *request) {
	size_t length = strlen(command);
	return request + length;
}

struct session *reserve_first_available_id(const struct aquarium *aquarium) {
	struct sessions_list *current;
	LIST_FOREACH(current, &SESSIONS, entries) {
		if (current->session->used == 0 && current->session->aquarium == aquarium) {
			current->session->used = 1;
			return current->session;
		}
	}
	return NULL;
}

void session_get_id(const struct session *session, char *id) {
	id[0] = session->type;
	sprintf(id + 1, "%d", session->id);
}

struct connection_data *session_get_connection(const struct session *session) {
	return (struct connection_data *) session->connection;
}

struct aquarium *session_get_aquarium(const struct session *session) {
	return (struct aquarium *) session->aquarium;
}

struct session *get_session_from_id(const char *id, const struct aquarium *aquarium) {
	check(is_valid_id(id), return NULL, "Invalid ID given to get session");
	int num = atoi(id + 1);
	check(exists_id(id[0], num, aquarium), return NULL, "Session not found for ID given");
	struct sessions_list *current;
	LIST_FOREACH(current, &SESSIONS, entries) {
		if (current->session->type == id[0] && current->session->id == num && current->session->aquarium == aquarium) {
			return current->session;
		}
	}
	return NULL;
}

struct session *reserve_id(char type, int id, const struct aquarium *aquarium) {
	struct sessions_list *current;
	LIST_FOREACH(current, &SESSIONS, entries) {
		if (current->session->type == type && current->session->id == id && current->session->aquarium == aquarium) {
			check(current->session->used != 1, return NULL, "Tried to reserve an already reserved ID");
			current->session->used = 1;
			return current->session;
		}
	}
	return NULL;
}

int add_id(const char *id, const struct aquarium *aquarium) {
	check(is_valid_id(id) == 1, return -1, "Invalid ID given for creation");
	int num = atoi(id + 1);
	check(exists_id(id[0], num, aquarium) != 1, return -1,
	      "Combination (id, aquarium) already exists. Failed to add new session");
	struct session *session = malloc(sizeof(struct session));
	session->type = id[0];
	session->id = num;
	session->used = 0;
	session->aquarium = aquarium;

	struct sessions_list *entry = malloc(sizeof(struct sessions_list));
	entry->session = session;
	LIST_INSERT_HEAD(&SESSIONS, entry, entries);

	return 0;
}

int delete_id(const char *id, const struct aquarium *aquarium) {
	check(is_valid_id(id), return -1, "Trying to remove an invalid ID");
	int num = atoi(id + 1);
	struct sessions_list *current;
	LIST_FOREACH(current, &SESSIONS, entries) {
		if (current->session->type == id[0] && current->session->id == num &&
		    current->session->aquarium == aquarium) {
			free(current->session);
			LIST_REMOVE(current, entries);
			return 0;
		}
	}
	log_message(ERROR, "Trying to remove a non-existing ID");
	return -1;
}

struct session *register_authentication(const char *request, char *reply, const struct connection_data *connection,
                                        const struct aquarium *aquarium) {
	enum request_type type = get_request_type(request);
	struct session *session;
	const char *id;

	switch (type) {

		case HELLO:
			session = reserve_first_available_id(aquarium);
			if (session == NULL) {
				log_message(WARN, "No ID available for reservation");
				strcpy(reply, "no greeting");
				return NULL;
			}
			session->connection = connection;
			sprintf(reply, "greeting %c%d", session->type, session->id);
			return session;

		case HELLO_AS:
			id = extract_id(request);
			session = reserve_id(id[0], atoi(id + 1), aquarium);
			if (session == NULL) {
				session = reserve_first_available_id(aquarium);
				if (session == NULL) {
					log_message(WARN, "No ID available for reservation");
					strcpy(reply, "no greeting");
					return NULL;
				} else {
					log_message(INFO, "ID requested was not available. Sending available instead.");
					session->connection = connection;
					sprintf(reply, "greeting %c%d", session->type, session->id);
					return session;
				}
			}
			session->connection = connection;
			sprintf(reply, "greeting %c%d", session->type, session->id);
			return session;

		case INVALID:
			log_message(ERROR, "Trying to register with a malformed authentication request");
			return NULL;

		default:
			// Should not go there.
			log_message(ERROR, "An unexpected error has occurred! Please contact the developers.");
			return NULL;
	}
}

int unregister_authentication(struct session *session, char *reply) {
	check(session->used == 1, return -1, "ID is not currently reserved");
	if (reply != NULL)
		strcpy(reply, "bye");
	session->connection = NULL;
	session->used = 0;
	return 0;
}

void
network_register_authentication(int argc, char **argv, struct aquarium *aquarium, struct connection_data *connection) {
	int max_length = 64;
	char *req = malloc(max_length * sizeof(char));
	char *reply = malloc(max_length * sizeof(char));
	for (int i = 0; i < argc - 1; ++i) {
		strcat(req, argv[i]);
		strcat(req, " ");
	}
	strcat(req, argv[argc - 1]);
	register_authentication(req, reply, connection, aquarium);
	strcat(reply, "\n"); // This is so that the reply message is parsed by the client on reception.
	network_reply(reply, connection);
}

void network_log_out(int argc, const char **argv, struct aquarium *aquarium, struct connection_data *connection) {
	int max_length = 64;
	char *req = malloc(max_length * sizeof(char));
	char *reply = malloc(max_length * sizeof(char));
	char *log_out_command = "log out as ";
	size_t length = strlen(log_out_command);

	for (int i = 0; i < argc - 1; ++i) {
		strcat(req, argv[i]);
		strcat(req, " ");
	}
	strcat(req, argv[argc - 1]);

	char *id = req + length;
	check(strncmp(req, log_out_command, length) == 0, return, "Received malformed packet for disconnection");
	check(is_valid_id(id) == 1, return, "Received incorrect ID for disconnection");
	struct session *session = get_session_from_id(id, aquarium);
	check(session != NULL, return, "ID wasn't found in authenticator");

	check(unregister_authentication(session, reply) == 0, return, "Could not disconnect client");
	strcat(reply, "\n"); // This is so that the reply message is parsed by the client on reception.
	network_reply(reply, connection);
}

struct session *get_session_from_connection(const struct connection_data *connection) {
	struct sessions_list *current;
	LIST_FOREACH(current, &SESSIONS, entries) {
		if (current->session->connection == connection) {
			return current->session;
		}
	}
	return NULL;
}
