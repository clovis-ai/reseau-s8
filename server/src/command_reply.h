#ifndef SERVER_COMMAND_REPLY_H
#define SERVER_COMMAND_REPLY_H
struct connection_data;

#define cli_reply(message, ...) printf("\t-> " message, ##__VA_ARGS__)

void network_reply(char *message, struct connection_data *connection);

void network_ok_reply(char *message, struct connection_data *connection);

void network_nok_reply(char *message, struct connection_data *connection);

#endif //SERVER_COMMAND_REPLY_H
