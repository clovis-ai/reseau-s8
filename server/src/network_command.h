#ifndef SERVER_NETWORK_COMMAND_H
#define SERVER_NETWORK_COMMAND_H

#include "command.h"
#include "aquarium.h"
#include "command_reply.h"
#include "connection.h"
#include "server.h"

void network_add_fish(int argc,
                      const char **argv,
                      struct aquarium *aquarium,
                      struct connection_data *connection);

void network_del_fish(int argc,
                      const char **argv,
                      struct aquarium *aquarium,
                      struct connection_data *connection);

void network_start_fish(int argc,
                        const char **argv,
                        struct aquarium *aquarium,
                        struct connection_data *connection);

void network_register_authentication(int argc, const char **argv, struct aquarium *aquarium,
                                     struct connection_data *connection);

void network_log_out(int argc,
                     const char **argv,
                     struct aquarium *aquarium,
                     struct connection_data *connection);

void network_ping(int argc,
                  const char **argv,
                  struct aquarium *aquarium,
                  struct connection_data *connection);

void network_get_fishes(int argc,
                        const char **argv,
                        struct aquarium *aquarium,
                        struct connection_data *connection);

void network_get_fishes_continuously(int argc,
                                     const char **argv,
                                     struct aquarium *aquarium,
                                     struct connection_data *connection);

#endif //SERVER_NETWORK_COMMAND_H
