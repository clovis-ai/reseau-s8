#ifndef RESEAU_ENSEIRB_S8_BEHAVIOR_H
#define RESEAU_ENSEIRB_S8_BEHAVIOR_H

#include "fish.h"

struct behavior;

/**
 * Returns a pointer to a behavior. Needs to be freed
 * @param mobility
 * @return NULL if invalid behavior name, a behavior otherwise
 */
struct behavior *get_behavior(const char *mobility);

/**
 * This function will add waypoints according to the fish's behavior
 * @param fish
 */
void mobility_add_waypoints(struct fish *fish, struct waypoint *last_waypoint);

#endif //RESEAU_ENSEIRB_S8_BEHAVIOR_H
