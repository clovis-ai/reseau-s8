#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "behavior.h"
#include "fish.h"
#include "position.h"
#include "debug.h"
#include "aquarium.h"

struct behavior {
	char *name;

	void (*generate_positions)(struct fish *fish, struct waypoint *last_waypoint);
};

void random_mobility(struct fish *fish, struct waypoint *last_waypoint) {
	int waypoints_to_add = 1;
	time_t waypoint_inter_duration = 2;
	int movement_box_size = 5;

	time_t last_waypoint_time;
	const struct absolute_position *last_position;

	// Checking if fish has a waypoint or not, and take position and timestamp of
	// waypoint and that of
	if (last_waypoint == NULL) {
		last_waypoint_time = time(NULL);
		last_position = fish_position(fish);
	} else {
		last_waypoint_time = waypoint_timestamp(last_waypoint);
		last_position = waypoint_position(last_waypoint);
	}

	const struct absolute_position *aqua_position = aquarium_position(fish_get_aquarium(fish));

	struct absolute_position new_position;
	time_t new_timestamp;
	new_position.height = last_position->height;
	new_position.width = last_position->width;
	for (int i = 0; i < waypoints_to_add; ++i) {
		do {
			// Computing a new position randomly in the box of size movement_box_size around the fish
			new_position.x = last_position->x +
			                 (((rand() % (2 * movement_box_size + 1)) - movement_box_size));
			new_position.y = last_position->y +
			                 (((rand() % (2 * movement_box_size + 1)) - movement_box_size));
		} while (!position_is_inside(&new_position, aqua_position));
		new_timestamp = last_waypoint_time + waypoint_inter_duration;
		add_waypoint(fish, &new_position, new_timestamp);
		last_position = &new_position;
		last_waypoint_time = new_timestamp;
	}
}

void horizontal_mobility(struct fish *fish __attribute__((unused)),
                         struct waypoint *last_waypoint __attribute__((unused))) {
	// TODO: see REQ-23
}

struct behavior behaviors[] = {
		{"RandomWayPoint",    random_mobility},
		{"HorizontalPathWay", horizontal_mobility}
};
int mobility_number = sizeof(behaviors) / sizeof(struct behavior);

struct behavior *get_behavior(const char *mobility) {
	struct behavior *behavior;
	size_t behavior_size = sizeof(*behavior);
	behavior = malloc(behavior_size);
	for (int i = 0; i < mobility_number; ++i) {
		if (strcmp(mobility, behaviors[i].name) == 0) {
			memcpy(behavior, behaviors + i, behavior_size);
			return behavior;
		}
	}
	error("Mobility model given not found in available behaviors");
	free(behavior);
	return NULL;
}

void mobility_add_waypoints(struct fish *fish, struct waypoint *waypoint) {
	fish_get_behavior(fish)->generate_positions(fish, waypoint);
}
