#ifndef SERVER_CONNECTION_H
#define SERVER_CONNECTION_H

#include "aquarium.h"
#include "command_reply.h"

struct session;

/**
 * This function adds an ID to the list of ID available for reservation by a client.
 * A session is unique for the pair (id, aquarium).
 * @param id A string formed by a letter followed by a number, e.g "N1", "A380"...
 * Types must be one of defined ones.
 * @param aquarium
 * @return 0 if everything was successful. -1 if a pair (id, aquarium) is already available
 * for reservation, or if the ID given is incorrect.
 */
int add_id(const char *id, const struct aquarium *aquarium);

/**
 * This function writes the ID contained in session in the buffer id.
 * @param session
 * @param id
 */
void session_get_id(const struct session *session, char *id);

/**
 * This function returns the aquarium associated to a session structure.
 * @param session
 * @return
 */
struct aquarium *session_get_aquarium(const struct session *session);

/**
 * This function returns the connection_data associated to an ID.
 * @param session
 * @return
 */
struct connection_data *session_get_connection(const struct session *session);

/**
 * This function registers an authentication request into the authenticator.
 * @param request in the form \"hello\" or \"hello in as <id>\"
 * @param reply A buffer to write the response to give to the requesting client.
 * @param connection
 * @param aquarium
 * @return NULL if reservation was impossible. A session otherwise.
 */
struct session *register_authentication(const char *request, char *reply, const struct connection_data *connection,
                                        const struct aquarium *aquarium);

/**
 * This function makes a reserved ID available for reservation.
 * @param session
 * @param request
 * @param reply
 * @return -1 if the ID is not already used, or if the request is not made correctly. 0 otherwise.
 */
int unregister_authentication(struct session *session, char *reply);

/**
 * This function deletes the ID associated with session and frees the associated ressources.
 * @param session
 * @return 0 if deletion was successful. -1 otherwise.
 */
int delete_id(const char *id, const struct aquarium *aquarium);

/**
 * This function returns the session register in the authenticator corresponding to the ID and the aquarium given.
 * @param id
 * @param aquarium
 * @return NULL if the ID is not valid or if the ID was not previously added to the authenticator.
 */
struct session *get_session_from_id(const char *id, const struct aquarium *aquarium);

/**
 * This function returns the first session matching the connection information given
 * @param connection
 * @return NULL if not found. A session otherwise.
 */
struct session *get_session_from_connection(const struct connection_data *connection);

#endif //SERVER_CONNECTION_H
