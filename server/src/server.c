#include <sys/socket.h>
#include <stdlib.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <time.h>
#include "pthread.h"
#include "server.h"
#include "debug.h"
#include "command_reply.h"
#include "command_parser.h"
#include "connection.h"

#define BUFF_SIZE 128
#define TIMEOUT_REFRESH_RATE 3

/**
 * The server_memory structure stores all the opened sockets and allocated memory to be respectively closed
 * and freed before exiting.
 */
struct server_memory {
	int main_socket;
	int sockets[MAXIMUM_WAITING_CONNECTIONS];
	int sockets_len;
	struct connection_data *connections[MAXIMUM_WAITING_CONNECTIONS];
	pthread_t threads[MAXIMUM_WAITING_CONNECTIONS];
	pthread_mutex_t *state_locks[MAXIMUM_WAITING_CONNECTIONS];
	pthread_mutex_t *lock;
};

/**
 * The connection_data structure stores everything used by clients to keep track of the connection and return
 * their status when they exit.
 */
struct connection_data {
	struct sockaddr_in *address;
	int address_length;
	int socket;
	int status;
	int thread_state;
	time_t last_message;
	pthread_mutex_t *state_lock;
};

struct server_timeout {
	struct server_memory *memory;
	const double *timeout;
};

/**
 * The EXIT_CODE enum serves as a status code when to_do_before_exit client exits.
 */
enum EXIT_CODE {
	CLIENT_EXIT_DISCONNECT, CLIENT_EXIT_ERR_RECV
};

enum THREAD_STATE {
	THREAD_NOT_COMPLETED, THREAD_COMPLETED
};

struct aquarium *aquarium;

static void *client_connection(void *connection) {
	struct connection_data *conn = (struct connection_data *) connection;
	size_t received_len;
	char buffer[BUFF_SIZE];

	while (true) {
		received_len = recv(conn->socket, buffer, BUFF_SIZE, 0);
		if (received_len == -1ul) {
			log_message(ERROR, "Could not receive the message on client socket");
			conn->status = CLIENT_EXIT_ERR_RECV;
			break;
		}

		if (received_len == 0) {
			// Somebody disconnected, get his details and print
			getpeername(conn->socket, (struct sockaddr *) conn->address,
			            (socklen_t *) &conn->address_length);
			log_message(INFO, "Host disconnected, socket %d, ip %s, port %d...", conn->socket,
			            inet_ntoa(conn->address->sin_addr), ntohs(conn->address->sin_port));
			conn->status = CLIENT_EXIT_DISCONNECT;
			break;
		} else {
			// Display the message that came in
			// Set the string terminating NULL byte on the end of the data read
			buffer[received_len] = '\0';
			log_message(INFO, "Message received on socket %d:\n\t%s", conn->socket, buffer);
			connection_set_time(conn);
			parse_and_execute_network(buffer, aquarium, conn);
		}
	}
	pthread_mutex_lock(conn->state_lock);
	conn->thread_state = THREAD_COMPLETED;
	pthread_mutex_unlock(conn->state_lock);
	pthread_exit((void *) conn);
}

static struct connection_data *
allocate_connection_data(struct sockaddr_in *address, int address_length, int socket,
                         pthread_mutex_t *state_lock) {
	struct connection_data *connection = malloc(sizeof(*connection));
	check(connection != NULL, exit(EXIT_FAILURE), "Malloc shouldn't fail");
	connection->address = address;
	connection->socket = socket;
	connection->address_length = address_length;
	connection->status = CLIENT_EXIT_DISCONNECT;
	connection->last_message = time(NULL);
	connection->thread_state = THREAD_NOT_COMPLETED;
	connection->state_lock = state_lock;
	return connection;
}

void close_connection(int index, struct server_memory *data) {
	shutdown(data->sockets[index], SHUT_RDWR);
	close(data->sockets[index]);
	data->connections[index] = NULL;
	free(data->connections[index]);
	data->sockets[index] = -1;
}

_Noreturn void *manage_disconnections(void *memory) {
	struct server_memory *data = (struct server_memory *) memory;
	int socket, thread_state;
	while (true) {
		for (int i = 0; i < data->sockets_len; ++i) {
			pthread_mutex_lock(data->lock);
			socket = data->sockets[i];
			pthread_mutex_unlock(data->lock);

			if (socket != -1) {
				pthread_mutex_lock(data->connections[i]->state_lock);
				thread_state = data->connections[i]->thread_state;
				pthread_mutex_unlock(data->connections[i]->state_lock);

				if (thread_state == THREAD_COMPLETED) {
					log_message(DEBUG, "A client exited, waiting for its status...");
					pthread_join(data->threads[i], (void *) data->connections[i]);
					switch (data->connections[i]->status) {
						case CLIENT_EXIT_DISCONNECT:
							log_message(INFO, "Client successfully disconnected...");
							break;
					}
					pthread_mutex_lock(data->lock);
					struct session *session = get_session_from_connection(data->connections[i]);
					if (session != NULL) {
						char id[10];
						session_get_id(session, id);
						log_message(INFO, "Session %s now available", id);
						unregister_authentication(session, NULL);
					}
					close_connection(i, data);
					pthread_mutex_unlock(data->lock);
				}
			}
		}
	}
}

_Noreturn void *timeout_disconnect(void *parameter) {
	struct server_timeout *server_timeout = (struct server_timeout *) parameter;
	time_t current_time;
	struct session *session;
	struct server_memory *memory = server_timeout->memory;
	const double *timeout = server_timeout->timeout;
	char id[64];
	while (1) {
		sleep(TIMEOUT_REFRESH_RATE);
		for (int i = 0; i < memory->sockets_len; ++i) {
			current_time = time(NULL);
			if (memory->sockets[i] != -1 &&
			    difftime(current_time, memory->connections[i]->last_message) >= *timeout) {
				session = get_session_from_connection(memory->connections[i]);
				if (session != NULL) {
					session_get_id(session, id);
					info("Client %s has timed-out. Disconnecting...", id);
					unregister_authentication(session, NULL);
				} else
					info("Unregistered client has timed-out. Disconnecting...");
				network_reply("bye\n", memory->connections[i]);
				close(memory->connections[i]->socket);
				while (memory->connections[i]->thread_state != THREAD_COMPLETED) { ;
				}
			}
		}
	}
}

_Noreturn void *start_server(const struct server_config *server_config) {
	if (pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL) != 0)
		log_message(WARN, "Could not enable thread-cancelling protection in server start");
	log_message(INFO, "Starting the server...");
	aquarium = server_config->aquarium;
	struct server_memory data;
	data.sockets_len = MAXIMUM_WAITING_CONNECTIONS;
	for (int i = 0; i < data.sockets_len; ++i) {
		data.sockets[i] = -1;
		data.connections[i] = NULL;
	}

	data.main_socket = socket(AF_INET, SOCK_STREAM, 0);
	check(data.main_socket != -1, server_clean(&data), "Could not create the main socket");
	log_message(DEBUG, "Main socket successfully created...");

	int call_result; // Stores the result of calls where the returned value only serves to check if succeed.
	int true_val = true;
	/* Allow the sockets to be reused if the server is shut down or to_do_before_exit client disconnects */
	call_result = setsockopt(data.main_socket, SOL_SOCKET, SO_REUSEADDR, &true_val, sizeof(int));
	check(call_result != -1, server_clean(&data), "Could not apply options on main socket");

	struct sockaddr_in local_addr = {.sin_family = AF_INET,
			/* Convert integer value from host byte order to network byte order */
			.sin_addr.s_addr = htonl(INADDR_ANY),
			.sin_port = htons(server_config->config.controller_port)};

	call_result = bind(data.main_socket, (struct sockaddr *) &local_addr, sizeof(local_addr));
	check(call_result != -1, server_clean(&data), "Could not bind the main socket");
	log_message(DEBUG, "Main socket successfully bound on port %ld...",
	            server_config->config.controller_port);

	call_result = listen(data.main_socket, data.sockets_len);
	check(call_result != -1, server_clean(&data), "Could not listen on main socket");
	log_message(INFO, "Server listening...");

	double timeout = (double) server_config->config.display_timeout_value;
	struct server_timeout server_timeout = {&data, (const double *) &timeout};

	int new_socket;
	int local_addr_len = sizeof(local_addr);

	pthread_mutex_t data_lock;
	pthread_mutex_init(&data_lock, NULL);
	data.lock = &data_lock;
	pthread_mutex_t state_locks[MAXIMUM_WAITING_CONNECTIONS];
	for (int i = 0; i < data.sockets_len; ++i) {
		pthread_mutex_init(&state_locks[i], NULL);
		data.state_locks[i] = &state_locks[i];
	}

	pthread_t connection_handler;
	pthread_create(&connection_handler, NULL, &manage_disconnections, (void *) &data);
	pthread_detach(connection_handler);

	pthread_t timeout_disconnecter;
	pthread_create(&timeout_disconnecter, NULL, &timeout_disconnect, (void *) &server_timeout);

	int socket;
	if (pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL) != 0)
		log_message(WARN,
		            "Could not remove thread-cancelling protection. Server might not be able to quit.");
	pthread_cleanup_push(server_clean, &data) ;

			while (true) {
				new_socket = accept(data.main_socket, (struct sockaddr *) &local_addr,
				                    (socklen_t *) &local_addr_len);
				check(new_socket != -1, server_clean(&data),
				      "Could not accept incoming connection");

		// Inform user of socket number - used in send and receive commands
		log_message(INFO, "New connection, socket fd is %d, ip is %s, port is %d...",
		            new_socket, inet_ntoa(local_addr.sin_addr), ntohs(local_addr.sin_port));

		int free_slot = false;
		// Add the new socket to array of sockets
		for (int i = 0; i < data.sockets_len; i++) {
			// Lock the data so that we don't conflict with to_do_before_exit disconnecting client
			pthread_mutex_lock(data.lock);
			socket = data.sockets[i];
			// If position is empty
			if (socket == -1) {
				free_slot = true;
				data.sockets[i] = new_socket;
				log_message(DEBUG, "Added new client to list of sockets as %d...", i);
				data.connections[i] = allocate_connection_data(&local_addr, local_addr_len,
				                                               new_socket, data.state_locks[i]);
				pthread_create(&data.threads[i], NULL, &client_connection,
				               (void *) data.connections[i]);
			}
			pthread_mutex_unlock(data.lock); // Unlock the data
			if (socket == -1) // If an empty slot was used then job done
				break;
		}
		if (free_slot == false) {
			log_message(ERROR, "All slots taken, connection refused to socket %d", new_socket);
			shutdown(new_socket, SHUT_RDWR);
			close(new_socket);
		}
	}
	pthread_cleanup_pop(0);
}

void connection_set_time(struct connection_data *connection) {
	connection->last_message = time(NULL);
}

void server_clean(void *memory) {
	log_message(INFO, "Closing all sockets...");
	struct server_memory *data = (struct server_memory *) memory;
	pthread_mutex_lock(data->lock);
	for (int i = 0; i < data->sockets_len; ++i) {
		close_connection(i, data);
	}
	shutdown(data->main_socket, SHUT_RDWR);
	close(data->main_socket);
	pthread_mutex_unlock(data->lock);
	log_message(INFO, "Memory cleaned...");
	exit(EXIT_SUCCESS);
}

void network_reply(char *message, struct connection_data *connection) {
	pthread_mutex_lock(connection->state_lock);
	if (connection->thread_state == THREAD_NOT_COMPLETED) {
		send(connection->socket, message, strlen(message), 0);
	}
	pthread_mutex_unlock(connection->state_lock);
}

void network_ok_reply(char *message, struct connection_data *connection) {
	char *text = "OK %s";
	char *buffer;
	format_string(buffer, text, message);
	network_reply(buffer, connection);
	free(buffer);
}

void network_nok_reply(char *message, struct connection_data *connection) {
	char *text = "NOK : %s";
	char *buffer;
	format_string(buffer, text, message);
	network_reply(buffer, connection);
	free(buffer);
}

void network_ping(int argc,
                  const char **argv,
                  struct aquarium *client_aquarium __attribute__((unused)),
                  struct connection_data *connection) {
	check(argc == 2, return, "Invalid number of argument given for ping");
	char *end;
	unsigned long random = strtol(argv[1], &end, 10);
	check(*end == 0, return, "Ping from client wasn't a number");
	check(get_session_from_connection(connection) != NULL, return, "Ping from unregistered client");

	char reply[64];
	sprintf(reply, "pong %lu\n", random);
	network_reply(reply, connection);
}
