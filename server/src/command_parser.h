#ifndef SERVER_COMMAND_PARSER_H
#define SERVER_COMMAND_PARSER_H

#include "aquarium.h"
#include "command_reply.h"

/**
 * Parses and executes a Command Line Interface command
 * Displays an error message if the command is invalid
 * @param buffer The string containing the command to treat
 */
int parse_and_execute_cli(const char *buffer, struct aquarium *aquarium);

/**
 * Parses and executes a Network command
 * Displays an error message if the command is invalid
 * @param buffer The string containing the command to treat
 */
int parse_and_execute_network(const char *buffer, struct aquarium *aquarium,
                              struct connection_data *connection);

#endif //SERVER_COMMAND_PARSER_H
