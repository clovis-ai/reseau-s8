#ifndef SERVER_LOADER_H
#define SERVER_LOADER_H

#define LOG_PATH_SIZE 50

struct config {
	long controller_port;
	long display_timeout_value;
	long fish_update_interval;
	char log_location[LOG_PATH_SIZE];
};

void config_load(struct config *target);

#endif //SERVER_LOADER_H
