#include <string.h>
#include <stdio.h>
#include "debug.h"
#include "command_parser.h"
#include "command.h"
#include "cli_command.h"
#include "network_command.h"
#include "aquarium.h"

//region HELP Parser

#define NET_ADD_FISH_USAGE "addFish <fish name> at <x position>x<y position>,<width>x<height>, <pathway>"
#define NET_ADD_FISH_DESCRIPTION "Adds a fish to the aquarium."
#define NET_DEL_FISH_USAGE "delFish <fish name>"
#define NET_DEL_FISH_DESCRIPTION "Deletes a fish."
#define NET_START_FISH_USAGE "startFish <name>"
#define NET_START_FISH_DESCRIPTION "Starts a fish."
#define NET_REGISTER_AUTHENTICATION_USAGE "hello | hello in as <id>"
#define NET_REGISTER_AUTHENTICATION_DESCRIPTION "Reserves an ID into the authenticator."
#define NET_UNREGISTER_AUTHENTICATION_USAGE "log out as <id>"
#define NET_UNREGISTER_AUTHENTICATION_DESCRIPTION "Disconnects a client and make the ID available for reservation"
#define NET_PING_USAGE "ping <number>"
#define NET_PING_DESCRIPTION "registers and responds to ping"
#define NET_GETFISHESCONTINUOUSLY_USAGE "getFishesContinuously"
#define NET_GETFISHESCONTINUOUSLY_DESCRIPTION "Subscribes to updates about the fish this view can see"
#define NET_GETFISHES_USAGE "getFishes"
#define NET_GETFISHES_DESCRIPTION "Send an update about the fish this view can see"

#define CLI_HELP_USAGE "help"
#define CLI_HELP_DESCRIPTION "Displays all the available commands."
#define CLI_SHOW_AQUARIUM_USAGE "show aquarium"
#define CLI_SHOW_AQUARIUM_DESCRIPTION "Shows a description of the aquarium."
#define CLI_SAVE_AQUARIUM_USAGE "save <aquarium file>"
#define CLI_SAVE_AQUARIUM_DESCRIPTION "Saves the aquarium into a file."
#define CLI_LOAD_AQUARIUM_USAGE "load <aquarium file>"
#define CLI_LOAD_AQUARIUM_DESCRIPTION "Loads an aquarium from a file."
#define CLI_ADD_VIEW_USAGE "add view <name> <x position>x<y position>+<width>+<height>"
#define CLI_ADD_VIEW_DESCRIPTION "Adds a view."
#define CLI_DEL_VIEW_USAGE "del view <name>"
#define CLI_DEL_VIEW_DESCRIPTION "Deletes a view."

//endregion

struct command network_commands[] = {
		{"addFish",               network_add_fish,                NET_ADD_FISH_USAGE,                  NET_ADD_FISH_DESCRIPTION},
		{"delFish",               network_del_fish,                NET_DEL_FISH_USAGE,                  NET_DEL_FISH_DESCRIPTION},
		{"startFish",             network_start_fish,              NET_START_FISH_USAGE,                NET_START_FISH_DESCRIPTION},
		{"hello",                 network_register_authentication, NET_REGISTER_AUTHENTICATION_USAGE,   NET_REGISTER_AUTHENTICATION_DESCRIPTION},
		{"log out",               network_log_out,                 NET_UNREGISTER_AUTHENTICATION_USAGE, NET_UNREGISTER_AUTHENTICATION_DESCRIPTION},
		{"ping",                  network_ping,                    NET_PING_USAGE,                      NET_PING_DESCRIPTION},
		{"getFishesContinuously", network_get_fishes_continuously, NET_GETFISHESCONTINUOUSLY_USAGE,     NET_GETFISHESCONTINUOUSLY_DESCRIPTION},
		{"getFishes",             network_get_fishes,              NET_GETFISHES_USAGE,                 NET_GETFISHES_DESCRIPTION}
};
static int number_of_network_commands = sizeof(network_commands) / sizeof(struct command);

struct command cli_commands[] = {
		{"help",          cli_help,          CLI_HELP_USAGE,          CLI_HELP_DESCRIPTION},
		{"show aquarium", cli_show_aquarium, CLI_SHOW_AQUARIUM_USAGE, CLI_SHOW_AQUARIUM_DESCRIPTION},
		{"save",          cli_save_aquarium, CLI_SAVE_AQUARIUM_USAGE, CLI_SAVE_AQUARIUM_DESCRIPTION},
		{"load",          cli_load_aquarium, CLI_LOAD_AQUARIUM_USAGE, CLI_LOAD_AQUARIUM_DESCRIPTION},
		{"add view",      cli_add_view,      CLI_ADD_VIEW_USAGE,      CLI_ADD_VIEW_DESCRIPTION},
		{"del view",      cli_del_view,      CLI_DEL_VIEW_USAGE,      CLI_DEL_VIEW_DESCRIPTION}
};
static int number_of_cli_commands = sizeof(cli_commands) / sizeof(struct command);

void cli_help(int argc,
              const char **argv __attribute__((unused)),
              struct aquarium *aquarium __attribute__((unused)),
              struct connection_data *connection __attribute__((unused))) {
	if (argc == 1) {
		printf("CLI Commands: \n");
		for (int i = 0; i < number_of_cli_commands; i++)
			printf("\t%s\n\t\t%s\n", cli_commands[i].usage, cli_commands[i].description);
	} else {
		log_message(ERROR, "Too many arguments\n%s", cli_commands[0].usage);
		return;
	}
	printf("\t%s\n\t\t%s\n", "quit", "Shuts down the server, disconnecting every connected client.");
}

int parse_and_execute(const char *buffer,
                      int number_of_commands,
                      struct command commands[],
                      struct aquarium *aquarium,
                      struct connection_data *connection) {
	char *tokens[strlen(buffer)];
	int last_was_space = 1;
	char buffer_copy[strlen(buffer)];
	strcpy(buffer_copy, buffer);

	for (int i = 0; i < number_of_commands; i++) {
		const char *command_name = commands[i].name;
		const size_t command_name_length = strlen(command_name);
		if (!strncmp(buffer, command_name, command_name_length)) {
			int argc = 1;
			char *car;
			for (car = buffer_copy + command_name_length; *car; car++)
				if (*car == ' ') {
					*car = 0;
					last_was_space = 1;
					argc++;
				} else if (last_was_space) {
					tokens[argc - 1] = car;
					last_was_space = 0;
				}
			*(car - 1) = 0; // Remove final \n
			tokens[0] = (char *) commands[i].name;
			const char **const_tokens = (const char **) tokens;
			commands[i].handler(argc, const_tokens, aquarium, connection);
			return 1;
		}
	}
	log_message(ERROR, "Command not recognized: %s", buffer);
	return 0;
}

int parse_and_execute_cli(const char *buffer, struct aquarium *aquarium) {
	return parse_and_execute(buffer, number_of_cli_commands, cli_commands, aquarium, NULL);
}

int parse_and_execute_network(const char *buffer, struct aquarium *aquarium,
                              struct connection_data *connection) {
	return parse_and_execute(buffer, number_of_network_commands, network_commands, aquarium,
	                         connection);
}
