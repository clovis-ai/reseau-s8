#ifndef SERVER_FISH_H
#define SERVER_FISH_H

#include <time.h>
#include "position.h"

struct waypoint;

struct fish;

#include "aquarium.h"

/**
 * Heap-allocates a fish.
 * @param name The name of the fish
 * @param starting_position The fish's position
 * @return A newly allocated fish.
 * @see fish_free
 */
struct fish *fish_create(const char *name,
                         const struct absolute_position *starting_position,
                         const struct aquarium *aquarium,
                         const char *behavior);

/**
 * Heap-deallocates a fish.
 * @param fish The fish to be freed
 * @see fish_create
 */
void fish_free(struct fish *fish);

/**
 * Gets the fish's name.
 * @param fish The fish in question
 * @return The fish's name.
 */
const char *fish_name(const struct fish *fish);

/**
 * Gets the fish's position.
 * @param fish The fish in question
 * @return The fish's position.
 */
const struct absolute_position *fish_position(const struct fish *fish);

/**
 * Starts the fish
 * @param fish
 */
void fish_start(struct fish *fish);

/**
 * Gets the next waypoint assigned to this fish.
 * @param fish
 * @return The next waypoint, or NULL if there isn't any.
 */
struct waypoint *next_waypoint(struct fish *fish);

/**
 * Gets the next waypoints assigned to this fish.
 * @param fish
 * @param output An array in which the waypoints will be added
 * @param size The number of waypoints requested
 * @return The number of waypoints added to 'output'. Cannot be greater than 'size', but could be lesser if there are not enough waypoints assigned.
 */
size_t next_waypoints(struct fish *fish, struct waypoint *output, size_t size);

/**
 * Adds a waypoint to this fish.
 * @param fish
 * @param position The position of that waypoint
 * @param timestamp When the fish should reach that position
 * @return 0 if everything went well, an error value otherwise
 */
int add_waypoint(struct fish *fish, struct absolute_position *position, time_t timestamp);

/**
 * The absolute position of a waypoint.
 */
const struct absolute_position *waypoint_position(struct waypoint *waypoint);

/**
 * The time at which the fish should reach that waypoint.
 *
 * You can use time(2) to get another time_t instance to compare with this one.
 */
time_t waypoint_timestamp(struct waypoint *waypoint);

/**
 * Indicates if a waypoint is no longer valid
 * @param waypoint
 * @return
 */
int is_waypoint_obsolete(struct waypoint *waypoint);

/**
 * Waypoint remaining time expressed as a signed value
 * @param waypoint
 * @return
 */
long waypoint_remaining_time(struct waypoint *waypoint);

/**
 * Returns the behavior model of a fish
 * @param fish
 * @return
 */
struct behavior *fish_get_behavior(const struct fish *fish);

/**
 * This function returns the aquarium the fish is in
 * @param fish
 * @return
 */
const struct aquarium *fish_get_aquarium(const struct fish *fish);

#endif //SERVER_FISH_H
