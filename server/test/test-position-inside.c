#include "tests.h"
#include "position.h"

int main() {
	struct absolute_position frame = {.x = 0, .y = 0, .width = 10, .height = 10};
	struct absolute_position inside = {.x = 1, .y = 1, .width = 9, .height = 9};
	struct absolute_position outside = {.x = 20, .y = 20, .width = 1, .height = 1};
	struct absolute_position partially_outside = {.x = 5, .y = 5, .width = 10, .height = 10};

	test("should be inside", position_is_inside(&inside, &frame))
	test("should be outside", !position_is_inside(&outside, &frame))
	test("should be outside (partially)", !position_is_inside(&partially_outside, &frame))
}
