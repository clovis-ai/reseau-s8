#ifndef SERVER_TEST_UTILS_H
#define SERVER_TEST_UTILS_H

#include <stdlib.h>
#include <stdio.h>

#define test_start(name) printf("%s:%d\t" name "… ", __FILE__, __LINE__)

#define test_success() printf("SUCCESS\n")
#define test_failure() printf("FAILURE\n"); exit(EXIT_FAILURE)

#define assert(bool) if (bool) {test_success();} else {test_failure();}

#define test(name, bool) test_start(name); assert(bool)

#endif //SERVER_TEST_UTILS_H
