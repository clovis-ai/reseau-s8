#include <string.h>
#include "tests.h"
#include <connection.h>
#include <aquarium.h>
#include <command_reply.h>

int main() {
	char *ids[3] = {"N1", "N2", "N4"};
	struct aquarium *aquariums[2];
	aquariums[0] = (struct aquarium *) malloc(sizeof(int));
	aquariums[1] = (struct aquarium *) malloc(sizeof(int));
	struct connection_data *connection = (struct connection_data *) malloc(sizeof(int));

	/**
	 * Adding (id, authenticator) pairs to the authenticator.
	 * The following IDS are added :
	 * (N1, aquarium[0]), (N1, aquarium[1]), (N2, aquarium[0]), (N4, aquarium[1])
	 */
	test("Adding (\"N1\", aquariums[0]) (should be successful)", add_id(ids[0], aquariums[0]) == 0);
	test("Adding (\"N1\", aquariums[1]) (should be successful)", add_id(ids[0], aquariums[1]) == 0);
	test("Trying to get session related to (\"N2\", aquariums[0]) (should be unsuccessful)",
	     get_session_from_id(ids[1], aquariums[0]) == NULL);
	test("Adding (\"N2\", aquariums[0]) (should be successful)", add_id(ids[1], aquariums[0]) == 0);
	test("Trying to get session related to (\"N2\", aquariums[0]) (should be successful)",
	     get_session_from_id(ids[1], aquariums[0]) != NULL);
	test("Adding (\"N4\", aquariums[1]) (should be successful)", add_id(ids[2], aquariums[1]) == 0);
	test("Adding (\"N1\", aquariums[0]) again (should be unsuccessful)", add_id(ids[0], aquariums[0]) == -1);
	test("Adding (\"A4\", aquariums[1]) (A is not a defined type, should be unsuccessful)",
	     add_id("A4", aquariums[1]));
	test("Removing (\"N1\", aquariums[0]) (should be successful)", delete_id(ids[0], aquariums[0]) == 0);
	test("Removing (\"N1\", aquariums[0]) again (should be unsuccessful)", delete_id(ids[0], aquariums[0]) == -1);
	test("Adding (\"N1\", aquariums[0]) again but after deletion (should be successful)",
	     add_id(ids[0], aquariums[0]) == 0);
	printf("\n");

	// Correctly formed packet for ("N1", aquariums[0]) - should reserve N1
	char *req = "hello in as N1";
	char id[64];
	char *expected_id = "N1";
	struct aquarium *aquarium = aquariums[0];
	char *expected_reply = "greeting N1";
	char reply[64];
	struct session *session = register_authentication(req, reply, connection, aquarium);
	session_get_id(session, id);
	test("Registering (\"N1\", aquariums[0]) (expected reply is \"greeting N1\")", strcmp(reply, expected_reply) == 0);
	test("Registering (\"N1\", aquariums[0]) (expected aquarium is aquariums[0])",
	     session_get_aquarium(session) == aquarium);
	test("Registering (\"N1\", aquariums[0]) (expected id is \"N1\")", strcmp(id, expected_id) == 0);
	printf("\n");

	// Correctly formed packet for ("N1", aquariums[0]), but it is already registered. Should reserve ("N2", aquariums[0]).
	expected_reply = "greeting N2";
	struct session *session1 = register_authentication(req, reply, connection, aquarium);
	expected_id = "N2";
	session_get_id(session1, id);
	test("Registering (\"N1\", aquariums[0]) again, but it is already registered. Sending available instead. (expected reply is \"hello in as N2\")",
	     strcmp(reply, expected_reply) == 0);
	test("Registering (\"N1\", aquariums[0]) again, but it is already registered. Sending available instead. (expected aquarium is aquariums[0])",
	     session_get_aquarium(session1) == aquarium);
	test("Registering (\"N1\", aquariums[0]) again, but it is already registered. Sending available instead. (expected ID is N2)",
	     strcmp(id, expected_id) == 0);
	printf("\n");

	// Unregistering ("N1", aquariums[0]). Should unregister.
	expected_reply = "bye";
	test("Unregistering (\"N1\", aquariums[0]) (should be successful)",
	     unregister_authentication(session, reply) == 0);
	test("Unregistering (\"N1\", aquariums[0]) (reply should be \"bye\")", strcmp(reply, expected_reply) == 0);
	test("Unregistering (\"N1\", aquariums[0]) again (should be unsuccessful)",
	     unregister_authentication(session, reply) == -1);
	printf("\n");

	// Registering ("N1", aquariums[0]) again. Trying with an incorrectly formed packet first.
	req = "hellp in as N1";
	test("Incorrectly formed packet for registration \"hellp in as N1\" (should not reserve)",
	     register_authentication(req, reply, connection, aquarium) == NULL);
	req = "hello in as N1";
	expected_reply = "greeting N1";
	expected_id = "N1";
	connection = (struct connection_data *) malloc(sizeof(int));
	session = register_authentication(req, reply, connection, aquarium);
	session_get_id(session, id);
	test("Registering (\"N1\", aquariums[0]) again after unregistration (expected reply is \"greeting N1\")",
	     strcmp(reply, expected_reply) == 0);
	test("Registering (\"N1\", aquariums[0]) again after unregistration (expected aquarium is aquariums[0])",
	     session_get_aquarium(session) == aquarium);
	test("Registering (\"N1\", aquariums[0]) again after unregistration (expected id is \"N1\")",
	     strcmp(id, expected_id) == 0);
	test("Getting session associated with connection_data (expected is (\"N1\", aquariums[0]))",
	     get_session_from_connection(connection) == session);
	test("Getting session associated to random connection (expected is NULL)",
	     get_session_from_connection((struct connection_data *) malloc(sizeof(int))) == NULL);
	test("Trying to get session associated to (\"N1\", aquariums[0]) (expected is session from register_session call)",
	     session == get_session_from_id(expected_id, aquarium));
	printf("\n");

	// Registering ("N1", aquariums[1]). Should register since it is not the same aquarium.
	req = "hello in as N1";
	expected_reply = "greeting N1";
	aquarium = aquariums[1];
	expected_id = "N1";
	session = register_authentication(req, reply, connection, aquarium);
	session_get_id(session, id);
	test("Registering (\"N1\", aquariums[1]) (expected reply is \"greeting N1\")", strcmp(reply, expected_reply) == 0);
	test("Registering (\"N1\", aquariums[1]) (expected aquarium is aquariums[1])",
	     session_get_aquarium(session) == aquarium);
	test("Registering (\"N1\", aquariums[1]) (expected id is \"N1\")", strcmp(id, expected_id) == 0);
	printf("\n");

	// Trying hello requests. for aquariums[0], everything has been reserved already. Should not reserve.
	// For aquariums[1], should reserve ("N4", aquariums[1]).
	req = "hello";
	aquarium = aquariums[0];
	expected_reply = "no greeting";
	session = register_authentication(req, reply, connection, aquarium);
	test("Registering any free ID for aquariums[0]. Everything should be reserved. Should not reserve (session should be NULL)",
	     session == NULL);
	test("Registering any free ID for aquariums[0]. Everything should be reserved. Should not reserve (reply should be \"no greeting\")",
	     strcmp(reply, expected_reply) == 0);
	aquarium = aquariums[1];
	expected_reply = "greeting N4";
	expected_id = "N4";
	session = register_authentication(req, reply, connection, aquarium);
	session_get_id(session, id);
	test("Registering any free ID for aquariums[1]. Should reserve (\"N4\", aquariums[1]) (expected reply is \"greeting N4\")",
	     strcmp(reply, expected_reply) == 0);
	test("Registering any free ID for aquariums[1]. Should reserve (\"N4\", aquariums[1]) (expected aquarium is aquariums[1])",
	     session_get_aquarium(session) == aquarium);
	test("Registering any free ID for aquariums[1]. Should reserve (\"N4\", aquariums[1]) (expected id is \"N4\"",
	     strcmp(id, expected_id) == 0);
	test("Registering any free ID for aquariums[1]. Should reserve (\"N4\", aquariums[1]) (testing connection getter)",
	     session_get_connection(session) == connection);

}
