
#include <position.h>
#include <string.h>
#include "tests.h"

int main() {
	struct absolute_position frame = {.x = 5, .y = 5, .width = 10, .height = 10};
	struct absolute_position input = {.x = 10, .y = 10, .width = 5, .height = 5};
	struct client_position expected = {.x = 5, .y = 5, .widthPercent = 50, .heightPercent = 50};
	struct client_position actual;

	position_to_relative(&input, &frame, &actual);

	test("should be equal", memcmp(&expected, &actual, sizeof expected) == 0)
}
