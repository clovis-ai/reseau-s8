% Preamble
\documentclass[11pt,french]{memoir}

% Packages
\usepackage{babel}
\usepackage{clovisai}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{xspace} % Pour ne pas mettre {} à la fin des guillemets

% Document
\begin{document}


	\title{Rapport intermédiaire de Projet de Réseau}
	\author{Maëlle Andricque, Mehdi Ben Salah, Ivan Canet, Raphaël Geynet, David Gond}
	\maketitle


	\section{Introduction : objectif du projet}\label{sec:intro}
	Ce projet consiste en la création d'un aquarium virtuel géré par un programme contrôleur appelé \og serveur \fg et qui fournit aux affichages appelés \og clients \fg les positions de chaque poisson à afficher, permettant ainsi de visualiser l'aquarium.
	Chaque client représente une portion de l'aquarium et permet à l'aide d'un invité de commande d'ajouter ou de supprimer des poissons de l'aquarium.
	Pour ce faire, chaque client dispose d'une connexion individuelle au serveur afin de communiquer des commandes et de recevoir les poissons à afficher.


	\section{Architecture}\label{sec:architecture}

	\subsection{Choix des technologies, présentation}\label{subsec:technologies-et-structure}

	\paragraph{Langage}

	Nous avons choisi d'implémenter le serveur en C, et le client en Kotlin.

	Kotlin est un langage orienté objet basé sur la JVM, comme Java.
	Créé par JetBrains, il connaît une grande croissance depuis quelques années, depuis que Google a annoncé que les APIs Android seraient écrites pour Kotlin, et non plus pour Java.

	Puisque Kotlin a accès aux APIs Java, ce projet est une bonne occasion pour découvrir le langage sans mettre en danger le projet : les classes utilisées sont exactement les mêmes que celles vues en cours.

	\paragraph{Structure du code}

	Nous avons choisi d'organiser le projet avec Gradle et CMake.

	Gradle est un des outils standards pour la gestion des projets Java et Android, avec Maven.
	Gradle peut aussi gérer des projets en C, mais nous ne l'avons jamais fait.
	Nous avons donc décidé de gérer la partie serveur avec CMake, que nous sommes habitués à utiliser, et de simplement dire à Gradle de déléguer cette partie du travail à CMake.

	Nous avons donc :
	\begin{lstlisting}[label={lst:fichiers}]
client/
	build.gradle.kts
server/
	CMakeLists.txt
	build.gradle.kts
build.gradle.kts
	\end{lstlisting}

	Le projet est séparé entre les tâches :
	\begin{itemize}
		\item \lstinline{server:run} Exécute le serveur (et le recompile, etc, au besoin, en déléguant à CMake)
		\item \lstinline{client:run} Exécute le client (et le recompile, etc, au besoin)
		\item \lstinline{install} Génère tous les fichiers (exécutables, fichiers de configuration\dots)
	\end{itemize}
	La liste complète des commandes est disponible avec \lstinline{tasks --all} (voir le \lstinline{README.md} pour plus d'informations).

	%TODO : architecture, découpage des fonctions

	%TODO : diagrammes

	\subsection{Architecture du serveur}\label{subsec:architecture-du-serveur}

	\paragraph{Positions}

	Les clients n'ont accès qu'à des positions relatives à l'intérieur de leur propre vue.
	À l'inverse, le serveur doit gérer les positions absolues ainsi que les positions relatives.

	Pour ce faire, le serveur a donc deux structures de données :
\begin{lstlisting}[label={lst:positions},language=c]
// position.h (simplified for readability)

struct absolute_position {
	unsigned int x;
	unsigned int y;
	unsigned int width;
	unsigned int height;
};

struct client_position {
	int x;
	int y;
	unsigned int widthPercent;
	unsigned int heightPercent;
};
\end{lstlisting}

	Ces deux structures font exactement la même taille.
	Nous utilisons deux structures différentes pour que le compilateur nous prévienne quand nous essayons de passer en paramètre une position absolue à la place d'une position relative (et vice versa).

	Le fichier contient ensuite un certain nombre de fonctions utilitaires de conversion (\lstinline{position_to_absolute}, \lstinline{position_to_relative}), de test (\lstinline{position_is_inside}), etc.

	Ici, la structure \lstinline{position} représente une donnée, et non une abstraction : elle n'est donc pas encapsulée dans les sources, mais est bien visible dans le header.

	\paragraph{Gestion des connexions}
	Le serveur possède de multiples threads, l'un d'entre eux écoute sur une socket afin de détecter une nouvelle connexion.

	Lorsqu'un client tente de se connecter au serveur, celui-ci reçoit la demande sur une socket réservée aux nouvelles connexions et s'il reste un emplacement de libre (autrement dit que le serveur peut accepter une nouvelle connexion) alors il conserve le descripteur associé et démarre un thread qui représente la connexion.
	Dans ce thread, le serveur attend de recevoir un message, le parse, exécute la commande associée, répond, puis recommence l'attente.

	Nous utilisons des mutex afin de synchroniser les multiples informations des connexions entre les threads.

	\paragraph{Gestion des déconnexions}
	Tout comme pour la gestion des connexions, le serveur possède un thread pour détecter les déconnexions.
	Ce thread utilise les mutex afin de se synchroniser et éviter de déconnecter prématurément un client sur une connexion fraîchement libérée et renouvelée.

	\subsection{Architecture du client}\label{subsec:architecture-du-client}

	\paragraph{Connexion TCP/IP}

	La connexion TCP/IP est gérée par la classe \lstinline{Client}.

	Cette classe possède deux fonctions importantes :
	\begin{itemize}
		\item \lstinline{dispatch} qui correspond à une boucle de lecture depuis la socket, jusqu'à ce qu'elle soit fermée
		\item \lstinline{send} qui correspond à l'envoi d'une commande, et l'attente (asynchrone) de la réponse
	\end{itemize}

	Cette architecture provient de l'existence de la commande \lstinline{ls} : le serveur peut envoyer des messages au client sans demande du client, il est donc important que le client écoute en permanence sur la socket, et pas juste quand il a envoyé une requête.

	Lorsque le client envoie une demande, on va l'ajouter à une file d'attente : de cette manière, quand on reçoit une réponse, on peut savoir à exactement à quelle commande la réponse correspond (parce que la socket est séquentielle).
	La file d'attente est protégée par un sémaphore.
	La requête est suspendue (placée en attente) tant qu'une réponse n'est pas reçue.

	\paragraph{Positions}

	Le client possède une interface \lstinline{Coordinate} avec deux coordonnées, une implémentation \lstinline{Point} qui l'implémente simplement, et une implémentation \lstinline{Position} qui utilise deux points pour représenter un rectangle (héritage par composition).

	Chaque implémentation possède aussi un certain nombre d'opérateurs pour faciliter leur utilisation.
	Les classes sont toutes immuables (pour simplifier le parallélisme et le debug).

	\paragraph{Lecture des fichiers}

	La JVM possède une manière standard de gérer les \emph{ressources} : elles sont stockées à l'intérieur du JAR lui-même.
	Ce fonctionnement est intégré automatiquement par Gradle : les fichiers dans \lstinline{src/main/resources} sont récupérés.

	Pour satisfaire les contraintes du projet (le fichier de configuration doit être dans le dossier dans lequel l'exécutable est lancé), nous avons créé la classe abstraite \lstinline{Resource}.
	Cette classe, ainsi que ses méthodes factory (\lstinline{resource} et \lstinline{resourceOrNull}) s'occupe de trouver le fichier, par priorité dans le dossier courant, sinon dans le JAR\@.


	\section{Gestion de projet}\label{sec:gestion-de-projet}
	Dès le début de la réalisation du projet, nous avons mis en place un dépôt GitLab afin de bénéficier entre autres des outils de gestion de tâches et de revue de code.
	Ce choix d'utilisation provient également du fait que GitLab nous offre toute la liberté de gestion du dépôt et des branches que ne nous permet pas la forge de l'école.
	Bien évidemment, la branche principale du dépôt GitLab est également envoyée sur le dépôt que nous avons sur la forge de l'école.

	Les premières séances du projet ont consisté en la séparation des différentes étapes du projet, à savoir la mise en place de l'architecture de construction du serveur et du client, la création des versions basiques du serveur et du client, les sockets TCP/IP et la gestion des connexions TCP, l'implémentation des différentes commandes console et réseau et enfin l'interface graphique du client.
	Nous avons réparti ces étapes en sprints de deux semaines afin de nous inciter à ne pas perdre de temps pour compléter le projet dans le délai imparti.

	La répartition des tâches s'est effectuée sur le tableau des tâches de GitLab (similaire à un Kanboard), où chacun s'attribue une tâche disponible puis la réalise sur une branche associée.
	Une fois la tâche réalisée, une revue de code est demandée afin de repérer de potentiels erreurs, bugs ou oublis.
	Nous avons défini un nombre minimum de deux revues acceptées avant de pouvoir ajouter le contenu sur la branche principale afin d'améliorer la détection des problèmes en amont.


	\section{Avancement}\label{sec:avancement}
	Au moment où ce rapport est écrit, nous bénéficions d'un serveur chargeant le fichier de configuration, pouvant accepter plusieurs connexions simultanées, est capable de récupérer les messages envoyés par chaque client, de les parser pour reconnaître les commandes associées et d'appeler la fonction associée à chaque commande.
	Toutes les commandes ne sont pas encore intégrées sur la branche principale, notamment celles qui manipulent les vues et l'aquarium, mais elles sont en cours de revue.
	Le serveur accepte également les entrées dans la ligne de commande, ces entrées sont traitées de manière similaire aux commandes réseau mais affichent leur réponse sur sa sortie standard au lieu d'envoyer sur une connexion.

	Du côté du client, il est capable de charger son fichier de configuration, se connecter au serveur à l'adresse indiquée dans la configuration, envoyer des messages et recevoir les réponses.
	Comme le serveur, il accepte également les entrées dans la ligne de commande.

	Enfin, nous avons mis en place sur le serveur et le client un système de log afin de conserver dans un nouveau fichier le déroulement de chaque exécution.
	Ces fichiers sont situés dans le dossier \texttt{server/cmake-build-debug/src/logs} et sont nommés par la date exacte~---~heure comprise~---~du lancement du serveur/client.


	\section{Reste à faire}\label{sec:reste}
	En supposant que les tâches en cours de revue soient approuvées et envoyées sur la branche principale, il nous reste à implémenter pour le client son interface graphique afin d'afficher les poissons, les commandes réseau de manipulation des poissons et d'affichage du statut du client.

	Concernant le serveur, il lui manque également le support de la commande de statut ainsi que la gestion des chemins des poissons.


	\section{Résumé de l'avancement}\label{sec:resume}
	En résumé, le client est capable de communiquer avec le serveur, toutes les commandes sont reconnues mais ne sont pas encore totalement implémentées.
	Nous avons une gestion partielle des vues, des poissons et de l'aquarium mais ce n'est pas encore utilisable et il manque l'interface graphique pour afficher les poissons.
\end{document}
