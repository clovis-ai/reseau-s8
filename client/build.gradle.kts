plugins {
	java
	kotlin("jvm") version "1.5.0"
	application

	// Generate a JAR with all dependencies
	id("com.github.johnrengelman.shadow") version "7.0.0"
}

repositories {
	jcenter()
}

dependencies {
	// Tests
	testImplementation("junit:junit:4.13")

	// Parallelism
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.3")
}

application {
	mainClass.set("fr.ipb.reseau.MainKt")
}

tasks.named<JavaExec>("run") {
	standardInput = System.`in`

	workingDir = buildDir
	environment("reseau_logs" to "latest_only")
}

//region Installation

task<Copy>("installJar") {
	from("build/libs/client-all.jar")
	into("../install/")
	rename("client-all.jar", "client.jar")

	dependsOn("shadowJar")
}

task<Copy>("installConfig") {
	from("src/main/resources/")
	into("../install")
}

tasks.register("install") {
	dependsOn("installJar", "installConfig")
}

//endregion
