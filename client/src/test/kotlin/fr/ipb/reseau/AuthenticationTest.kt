package fr.ipb.reseau

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Test

class AuthenticationTest {
	private val client: Client = Client(Configuration.controllerAddress, Configuration.controllerPort)

	@Test
	fun `Test authentication`() = runBlocking {
		val authentication = Authentication(Configuration.viewId, client)
		val clientListener = GlobalScope.launch {
			client.dispatch()
		}
		authentication.request()
		assertEquals("N1", authentication.id)
		assertTrue(authentication.authenticated)

		authentication.logOut()
		clientListener.join()
	}

	@Test
	fun `Test log out`() = runBlocking {
		val authentication = Authentication("N1", client)
		val clientListener = GlobalScope.launch {
			client.dispatch()
		}
		authentication.request()
		authentication.logOut()
		assertFalse(authentication.authenticated)

		clientListener.join()
	}
}

