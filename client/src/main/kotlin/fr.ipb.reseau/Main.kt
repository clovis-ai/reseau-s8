package fr.ipb.reseau

import fr.ipb.reseau.Logger.debug
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.system.exitProcess

suspend fun main() {
	// Example conversation with outputs from client
	Logger.init(Configuration.logs)
	debug("Controller address: " + Configuration.controllerAddress)
	debug("Id: " + Configuration.viewId)
	debug("Controller port: " + Configuration.controllerPort)
	debug("Display timeout value: " + Configuration.displayTimeoutValue)
	debug("Resources: " + Configuration.resources)
	debug("Logs: " + Configuration.logs)

	val view = View(ArrayList(), Position(Point(0, 0), Point(100, 100)))

	// Connect to the server
	val client = Client(Configuration.controllerAddress, Configuration.controllerPort, view)
	val authentication = client.authenticate(Configuration.viewId)
	client.start()
	authentication.request()
	client.sendAndForget("getFishesContinuously")

	Gui.use {
		it.createAndShow(view)
		repl(view to client)
		debug("REPL has ended")
	}

	try {
		authentication.logOut()
	} catch (e: IllegalStateException) {
	}

	// Wait for the socket to be closed before closing the JVM
	client.stop("The REPL has ended.")

	debug("Main terminated, good night.")
	exitProcess(0)
}

tailrec suspend fun repl(context: Context) {
	withContext(Dispatchers.IO) {
		@Suppress("BlockingMethodInNonBlockingContext")
		System.out.flush()
	}

	print("$ ")
	val text = readLine() ?: error("No text to read, the REPL has died.")

	if (text.startsWith("quit"))
		return

	try {
		CliCommand.parse(text, context)
	} catch (e: CliCommandException) {
		println("\t-> ${e.message}")
	} catch (e: IllegalArgumentException) {
		println("\t->  ${e.message}")
	} catch (e: IllegalStateException) {
		println("\t-> ${e.message}")
	}

	repl(context)
}
