package fr.ipb.reseau

/**
 * A 2D coordinate with a [x] and a [y] position.
 */
interface Coordinate {
	/**
	 * The position on the horizontal axis.
	 * The left of the screen is 0, positive towards the right.
	 */
	val x: Int

	/**
	 * The position on the vertical axis. The top of the screen is 0, positive towards the bottom.
	 */
	val y: Int
}

/**
 * A simple 2D point, with mathematical operators.
 */
data class Point(override val x: Int, override val y: Int) : Coordinate {

	/**
	 * The sum of this point and an [other] point.
	 * This is equivalent to a sum of vectors/translation.
	 */
	operator fun plus(other: Point) = Point(x + other.x, y + other.y)

	/**
	 * The difference between this point and an [other] point.
	 */
	operator fun minus(other: Point) = this + (-other)

	/**
	 * The opposite of this point.
	 */
	operator fun unaryMinus() = Point(-x, -y)

}

/**
 * A 2D rectangle (AABB) with a [x] and [y] coordinate, as well as a [widthPercent] and a [heightPercent].
 *
 * @property start The coordinate of the top-left of this AABB.
 * @property size The size of this AABB, in percent of the screen size.
 */
@Suppress("unused", "MemberVisibilityCanBePrivate")
data class Position(val start: Point, val size: Point) : Coordinate by start {

	/**
	 * The width of this AABB, in percentage of the total screen width.
	 * Convenience property to access `size.x`.
	 * @see size
	 */
	val widthPercent get() = size.x

	/**
	 * The height of this AABB, in percentage of the total screen height.
	 * Convenience property to access `size.y`.
	 * @see size
	 */
	val heightPercent get() = size.y

	/**
	 * Translates this AABB by the [other] point.
	 */
	operator fun plus(other: Point) = Position(start + other, size)

	/**
	 * Translates this AABB by the opposite of the [other] point.
	 */
	operator fun minus(other: Point) = Position(start - other, size)

}
