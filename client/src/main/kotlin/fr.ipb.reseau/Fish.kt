package fr.ipb.reseau

import fr.ipb.reseau.Resource.Companion.resource
import fr.ipb.reseau.Resource.Companion.resourceOrNull

/**
 * @property name The ID of the fish, see [type]
 * @property behavior The mobility pattern of the Fish (`null` when unknown)
 * @property currentPosition The 2D rectangle of the Fish (used by the GUI)
 * @property nextWaypoint The next position the fish will reach, and the deadline (number of seconds)
 */
data class Fish(
	val name: String,
	val behavior: String?,
	var currentPosition: Position,
	var nextWaypoint: Pair<Position, Long>?,
) {

	/**
	 * The type of the Fish given by [name] with the pattern [name] = [type]_Int
	 */
	val type = Type.Type(name.split("_")[0])

	/**
	 * Boolean that equals to True if the Fish started to move, and False otherwise
	 * False by default
	 */
	var started = false

	/**
	 * The type of the Fish
	 * @param type A string defining the type
	 */
	class Type private constructor(private val type: String) {

		/**
		 * The File associated with the Fish type
		 * equals to [type].png if the file exists, PoissonDefaut.png otherwise
		 */
		val file: Resource =
			resourceOrNull("${Configuration.resources}/$type.png")
				?: resource("${Configuration.resources}/PoissonDefaut.png")

		override fun equals(other: Any?) = this === other || other is Type && type == other.type
		override fun hashCode() = type.hashCode()
		override fun toString() = "Fish.Type($type, $file)"

		companion object {
			private val allTypes = HashMap<String, Type>()

			/**
			 * Factory that instantiates a type only if it hasn't been instantiated yet.
			 */
			fun Type(type: String): Type = allTypes.computeIfAbsent(type) { Fish.Type(it) }
		}
	}

	override fun equals(other: Any?) = this === other || other is Fish && other.name == name
	override fun hashCode(): Int = name.hashCode()
}
