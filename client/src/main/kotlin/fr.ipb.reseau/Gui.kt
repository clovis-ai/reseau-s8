package fr.ipb.reseau

import fr.ipb.reseau.Logger.debug
import java.awt.Graphics
import java.awt.Image
import java.awt.Toolkit
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.io.Closeable
import javax.imageio.ImageIO
import javax.swing.*

class Gui(title: String, view: View) : JFrame() {
	private val panel = FishesPanel(view.fishes)
	val frameSize = Point(800, 600)

	init {
		setTitle(title)
		defaultCloseOperation = DO_NOTHING_ON_CLOSE
		setSize(frameSize.x, frameSize.y)
		setLocationRelativeTo(null)
		add(panel)
		isVisible = true
	}

	inner class FishesPanel(private val fishes: MutableList<Fish>) : JPanel(), ActionListener {
		/**
		 * A cache of unscaled fish images.
		 */
		private val fishTypeToImageUnscaled = HashMap<Fish.Type, Image>()

		/**
		 * A cache of fish images.
		 * The key is composed of the type of the fish, and its dimension.
		 */
		private val fishTypeToImage = HashMap<Pair<Fish.Type, Point>, ImageIcon>()

		private val fishToLabel = HashMap<Fish, JLabel>()

		private val timer = Timer(100, this)

		private val backgroundImage =
			ImageIO.read(Resource.resource("${Configuration.resources}/background.jpeg").toUrl())

		init {
			timer.isRepeats = true
			timer.start()
			repaint()
			revalidate()
		}

		override fun paintComponent(graphics: Graphics?) {
			super.paintComponent(graphics)
			graphics?.drawImage(
				backgroundImage.getScaledInstance(
					width,
					height,
					Image.SCALE_SMOOTH
				), 0, 0, this
			)
			val toolkit: Toolkit = Toolkit.getDefaultToolkit()

			for (fish in fishes) {
				// If the type hasn't been loaded, load it
				fishTypeToImageUnscaled.computeIfAbsent(fish.type) { type ->
					toolkit.getImage(type.file.toUrl())
				}

				// Rescale the image, if necessary
				fishTypeToImage.computeIfAbsent(fish.type to fish.currentPosition.size) { (type, fishSize) ->
					val unscaledImage = fishTypeToImageUnscaled[type]
						?: error("The image should already have been loaded!")

					ImageIcon(
						unscaledImage.getScaledInstance(
							(width * fishSize.x) / 100,
							(height * fishSize.y) / 100,
							Image.SCALE_SMOOTH
						)
					)
				}

				// Create the JLabel for the fish, if necessary
				fishToLabel.computeIfAbsent(fish) {
					JLabel(
						fishTypeToImage[fish.type to fish.currentPosition.size]
							?: error("Couldn't find ImageIcon for the fish $fish")
					)
						.also { add(it) }
				}

				// Update all JLabels
				fishToLabel[fish]?.setLocation(
					fish.currentPosition.x * width / 100,
					fish.currentPosition.y * height / 100
				) ?: error("Couldn't find the JLabel associated to the fish $fish")
			}

			// Remove fishes that are now outside the screen
			fishToLabel.entries.removeAll(
				fishToLabel.entries.filter { (fish, _) -> fish !in fishes }
					.onEach { (_, label) -> remove(label) }
			)
		}

		override fun actionPerformed(ev: ActionEvent) {
			if (ev.source == timer) {
				repaint()
				revalidate()
			}
		}
	}

	companion object : Closeable {
		private lateinit var frame: Gui

		@JvmStatic
		fun createAndShow(view: View) {
			frame = Gui("Némo's aquarium", view)
			frame.isVisible = true
		}

		@JvmStatic
		override fun close() {
			if (::frame.isInitialized) {
				debug("Closing the UI…")
				frame.dispose()
			}
		}
	}
}
