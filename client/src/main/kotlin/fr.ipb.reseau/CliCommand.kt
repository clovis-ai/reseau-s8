package fr.ipb.reseau

import fr.ipb.reseau.Logger.debug

typealias Context = Pair<View, Client>

object CliCommand {

	/**
	 * The list of commands.
	 *
	 * ```
	 * // For simple commands, use the lambda syntax:
	 * command(name, description) { args, context ->
	 *    println(do something …)
	 * }
	 *
	 * // For complicated commands, define a function somewhere and use a function reference:
	 * command(name, description, ::function)
	 * ```
	 */
	private val commands = mapOf(
		command("help", "Displays the list of commands and their usage", ::displayHelp),
		command("status", "displays the current fish list", ::displayStatus),
		command("addFish", "Adds a fish", ::addFish),
		command("startFish", "Starts a fish", ::startFish),
		command("delFish", "Removes a fish", ::delFish),
		command("getFishes", "Updates the fishes", ::getFishes)
	)

	suspend fun parse(request: String, context: Context) {
		debug("Parsing command: $request")

		val requestList = request.trim(' ').split(' ')
		val params = requestList.drop(1)

		commands[requestList[0]]?.let { (_, action) -> action(params, context) }
			?: throw CliCommandException("Command not found: " + requestList[0])
	}

	private fun displayHelp(
		@Suppress("UNUSED_PARAMETER") args: List<String>,
		@Suppress("UNUSED_PARAMETER") context: Context
	) {
		println("List of commands:")

		for ((name, command) in commands) {
			val (description, _) = command

			println("• $name: $description")
		}
		println("• quit: Disconnects from the server and closes the application")
	}

	private fun displayStatus(
		@Suppress("UNUSED_PARAMETER") args: List<String>, context: Context
	) {
		val (view, client) = context
		require(client.isAlive) { "This client has already died, cannot send a message." }
		println("\t-> OK : Connecté au contrôleur, " + view.fishes.size + " poissons trouvés ")
		for (fish in view.fishes) {
			val started = if (fish.started) "started" else "notStarted"
			println(
				"\tFish " + fish.name + " at " + fish.currentPosition.x + "x" + fish.currentPosition.y + ","
						+ fish.currentPosition.widthPercent + "x" + fish.currentPosition.heightPercent + " " + started
			)
		}
	}

	private suspend fun addFish(args: List<String>, context: Context) {
		require(args.size == 4) { "NOK : Wrong number of arguments, 4 expected, ${args.size} given, syntax should be : addFish <name> at <x>x<y>,<weight>x<heigth>, <behavior>" }
		val (view, client) = context
		val pos = args[2].split(',', 'x')
			.asSequence()
			.take(4)
			.map {
				it.toIntOrNull()
					?: error("NOK : Wrong syntax for position should be : <x>x<y>,<weight>x<height>, ")
			}
			.onEach { require(it >= 0) { "NOK : Integers representing position and size must be positive" } }
			.toList()
		require(pos[2] > 0 && pos[3] > 0) { "NOK : Integers representing size must be strictly positive" }
		require(pos[0] + pos[2] <= 100 && pos[1] + pos[3] <= 100) { "NOK : The fish must entirely fit inside the view" }
		val position = Position(Point(pos[0], pos[1]), Point(pos[2], pos[3]))

		val fish = Fish(args[0], args[3], position, null)
		val ret = client.addFish(fish)
		require(okCheck(ret)) { ret }
		view.fishes.add(fish)
		println("\t-> $ret")
	}

	private suspend fun startFish(args: List<String>, context: Context) {
		require(args.size == 1) { "Wrong number of arguments, 1 expected, ${args.size} given" }
		val (view, client) = context
		val foundFish = view.fishes.find { fish -> fish.name == args[0] }
		require(foundFish != null) { "Poisson inexistant : " + args[0] }
		val ret = client.startFish(foundFish)
		require(okCheck(ret)) { ret }
		println("\t-> $ret")
	}

	private suspend fun delFish(args: List<String>, context: Context) {
		require(args.size == 1) { "Wrong number of arguments 1 expected, ${args.size} given" }
		val (view, client) = context
		val fish =
			view.fishes.find { it.name == args[0] } ?: error("Poisson inexistant : ${args[0]}")

		val ret = client.removeFish(fish)
		require(okCheck(ret)) { ret }
		view.fishes.remove(fish)
		println("\t-> $ret")
	}

	private suspend fun getFishes(args: List<String>, context: Context) {
		require(args.isEmpty()) { "Wrong number of arguments 0 expected, ${args.size} given" }
		val (_, client) = context
		client.getFishes()
		println("\t-> OK : Poissons mis à jour")
		displayStatus(args, context)
	}
}

class CliCommandException(message: String, cause: Throwable? = null) :
	RuntimeException(message, cause)

private fun command(
	name: String,
	description: String,
	action: suspend (List<String>, Context) -> Unit
) = name to (description to action)
