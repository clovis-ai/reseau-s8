package fr.ipb.reseau

import fr.ipb.reseau.Logger.info
import fr.ipb.reseau.Logger.warn
import kotlinx.coroutines.*

/**
 * This class implements the function to perform an authentication request to a client.
 * It will also parse the answer so that the change is stored in the class.
 */
class Authentication(var id: String?, private val client: Client, parent: Job = Job()) {

	private val scope = CoroutineScope(parent + Dispatchers.Default)

	var authenticated: Boolean = false
		private set

	/**
	 * make an authentication request to the server using id specified on instantiation
	 * This method will not do anything if the client is already authenticated
	 */
	suspend fun request() {
		if (!authenticated) {
			val request = if (id?.isEmpty() != false) "hello" else "hello in as $id"
			processReply(client.send(request))
		} else {
			info("Client already authenticated")
		}
	}

	/**
	 * Make a log out request to the server.
	 *
	 * Will not do anything
	 * if the server replies in the wrong way or if client is not
	 * authenticated
	 */
	suspend fun logOut() {
		if (authenticated) {
			val reply = client.send("log out as $id")
			if (reply == "bye") {
				info("Successfully logged out!")
				authenticated = false
				scope.cancel("Logging out from server")
			} else {
				warn("Wrong reply from server")
			}
		} else info("Already logged out")
	}

	private fun processReply(reply: String) {
		val splitReply = reply.split(" ")
		when (splitReply[0]){
			"no" -> {
				throw RuntimeException("Server won't accept authentication.")
			}
			"greeting" -> {
				if (splitReply[1] != id) {
					info("Received id ${splitReply[1]} from server which is different than requested ($id).")
					id = splitReply[1]
				} else info("Authenticated as $id")
				authenticated = true
				scope.launch {
					client.keepAlive()
				}
			}
			else -> warn("Unrecognized reply from server")
		}
	}
}
