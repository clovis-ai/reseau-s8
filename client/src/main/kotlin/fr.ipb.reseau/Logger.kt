package fr.ipb.reseau

import java.io.File
import java.io.IOException
import java.io.PrintStream
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object Logger {

	private lateinit var output: File

	/**
	 * Logs a debug [message] to the output file if available or standard output if the file is not
	 * available or if [printToStandard] is set to true
	 * @param message The message to print
	 * @param printToStandard False means try to print to the outfile, true means to standard output
	 */
	fun debug(message: String, printToStandard: Boolean = false) =
		log("[DEBUG] $message", System.out, printToStandard)

	/**
	 * Logs an info [message] to the output file if available or standard output if the file is not
	 * available or if [printToStandard] is set to true
	 * @param message The message to print
	 * @param printToStandard False means try to print to the outfile, true means to standard output
	 */
	fun info(message: String, printToStandard: Boolean = false) =
		log("[INFO]  $message", System.out, printToStandard)

	/**
	 * Logs a warning [message] to the output file if available or standard error if the file is not
	 * available or if [printToStandard] is set to true
	 * @param message The message to print
	 * @param printToStandard False means try to print to the outfile, true means to standard error
	 */
	fun warn(message: String, printToStandard: Boolean = false) =
		log("[WARN]  $message", System.err, printToStandard)

	/**
	 * Initialises the file used to log the current execution. Does nothing if the path is
	 * not accessible
	 * @param path The path of the log folder
	 */
	fun init(path: String) {
		if (!isPathAvailable(path)) {
			warn("Given path is not accessible: $path, defaulting to standard outputs", true)
			return
		}

		val filename =
			if (System.getenv("reseau_logs") == "latest_only") "latest.txt"
			else createFileNameWithDate(LocalDateTime.now(), "yyyyMMdd-kkmmss", ".txt")
		output = File("$path/$filename")
		output.createNewFile()
		if (isFileAvailable()) {
			info("Log file ${output.absolutePath} created, using it to log", true)
		} else {
			warn("No log file available for this session, defaulting to standard outputs", true)
		}
	}

	private fun log(message: String, defaultOut: PrintStream, printToStandard: Boolean = false) {
		if (!printToStandard && isFileAvailable()) {
			output.appendText("$message\n")
		} else {
			defaultOut.println(message)
		}
	}

	private fun isFileAvailable(): Boolean {
		return ::output.isInitialized && output.exists() && output.canWrite()
	}

	private fun isPathAvailable(path: String): Boolean {
		try {
			Files.createDirectories(Paths.get(path))
		} catch (e: UnsupportedOperationException) {
			warn(e.stackTraceToString(), true)
			return false
		} catch (e: FileAlreadyExistsException) {
			warn(e.stackTraceToString(), true)
			return false
		} catch (e: IOException) {
			warn(e.stackTraceToString(), true)
			return false
		} catch (e: SecurityException) {
			warn(e.stackTraceToString(), true)
			return false
		}
		val folder = File(path)
		return folder.exists() && folder.isDirectory && folder.canExecute() && folder.canWrite()
	}

	private fun createFileNameWithDate(
		date: LocalDateTime,
		format: String,
		extension: String
	): String {
		val formatter = DateTimeFormatter.ofPattern(format)
		val formattedDate = date.format(formatter)
		return "$formattedDate$extension"
	}
}
