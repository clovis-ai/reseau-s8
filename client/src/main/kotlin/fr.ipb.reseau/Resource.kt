package fr.ipb.reseau

import fr.ipb.reseau.Logger.debug
import fr.ipb.reseau.Resource.Companion.resource
import fr.ipb.reseau.Resource.Companion.resourceOrNull
import java.io.Reader
import java.net.URL
import java.nio.charset.Charset
import java.io.File as JavaFile

/**
 * A file placed in the project's `src/main/resources` directory.
 *
 * Resources can also be placed in the same directory as where the project is executed,
 * in which case they override the bundled version.
 *
 * To access the contents of this resource, see [bufferedReader].
 *
 * To instantiate a resource, see factory methods [resource] and [resourceOrNull].
 */
sealed class Resource(val path: String) {

	/**
	 * Whether this resource can be found.
	 * - `true` if the resource was found in the current working directory or in the jar,
	 * - `false` if the resource wasn't found.
	 *
	 * If this value is `false`, this object is most likely unusable.
	 */
	abstract val exists: Boolean

	/**
	 * Instantiates a [Reader] to read the contents of this resource.
	 *
	 * This function expects the resource to [exists].
	 */
	abstract fun reader(
		charset: Charset = Charsets.UTF_8,
		bufferSize: Int = DEFAULT_BUFFER_SIZE
	): Reader

	abstract fun toUrl(): URL

	/**
	 * A [Resource] implementation reading from a [java.io.File] object.
	 */
	class File(path: String) : Resource(path) {

		private val file = JavaFile(path)

		override val exists = file.exists()

		init {
			debug("Instantiated $this")
		}

		override fun reader(charset: Charset, bufferSize: Int): Reader {
			ensureExists()
			return file.bufferedReader(charset, bufferSize)
		}

		override fun toUrl(): URL = file.toURI().toURL()

		override fun toString(): String {
			return "Resource(type=File, $path from '${file.absolutePath}', exists=$exists)"
		}
	}

	/**
	 * A [Resource] implementation reading from a resource inside the JAR, using [Class.getResource].
	 */
	class Jar(path: String) : Resource(path) {

		private val resource: URL? = javaClass.getResource("/$path")

		override val exists = resource != null

		init {
			debug("Instantiated $this")
		}

		override fun reader(charset: Charset, bufferSize: Int): Reader {
			ensureExists()
			return resource!!.readText(charset)
				.reader() // this requires to load the entire file into memory
		}

		override fun toUrl(): URL = resource ?: error("This resource cannot be found: $this")

		override fun toString(): String {
			return "Resource(type=Jar, $path from '$resource', exists=$exists)"
		}

	}

	companion object {

		/**
		 * Instantiates a [Resource] from a [path], or fails with an exception if the resource cannot be found.
		 *
		 * This function guarantees that the returned resource [exists].
		 *
		 * @see resourceOrNull
		 */
		fun resource(path: String): Resource =
			resourceOrNull(path) ?: error("Could not find the resource: $path")

		/**
		 * Instantiates a [Resource] from a [path], or returns `null` if the resource cannot be found.
		 *
		 * This function guarantees that the returned resource, if any, [exists].
		 *
		 * @see resource
		 */
		fun resourceOrNull(path: String): Resource? =
			sequenceOf(::File, ::Jar)
				.map { it(path) }
				.firstOrNull { it.exists }

	}
}

fun Resource.ensureExists() = check(exists) { "This resource cannot be found: $this" }
