package fr.ipb.reseau

import fr.ipb.reseau.Logger.debug
import fr.ipb.reseau.Logger.info
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withPermit
import java.io.PrintWriter
import java.net.Socket
import java.net.SocketException
import kotlin.coroutines.coroutineContext
import kotlin.random.Random

/**
 * Connects to the server/controller via TCP/IP.
 * @param ip The server's IP
 * @param port The server's port
 */
class Client(ip: String, port: Int, private val view: View) {
	private val socket = Socket(ip, port)
	private val socketSendStream = PrintWriter(socket.getOutputStream(), true)

	private val job = SupervisorJob()
	private val scope = CoroutineScope(job)

	private val requestQueue = ArrayDeque<CompletableDeferred<String>>()
	private val requestQueueLock = Semaphore(1)

	val isAlive get() = !job.isCancelled

	/**
	 * Starts the server in its own coroutine.
	 * @see stop
	 */
	fun start() = scope.launch {
		dispatch()
	}

	/**
	 * Listen for messages incoming from the server.
	 * This method does not return until the socket is closed.
	 *
	 * To send messages to the server, use [sendAsync].
	 */
	private suspend fun dispatch() {
		@Suppress("BlockingMethodInNonBlockingContext")
		withContext(Dispatchers.IO) {
			val input = socket.getInputStream().bufferedReader()

			try {
				info("Starting to listen for messages from the controller…")
				while (!socket.isClosed) {
					job.ensureActive()
					val answer = input.readLine() ?: break
					info("< $answer")
					when {
						answer.startsWith("list") -> {
							scope.launch {
                                handleFishUpdate(answer, view to this@Client)
                            }
						}
						answer.startsWith("bye") -> {
							stop("Received bye message from server")
						}
						else -> {
							requestQueueLock.withPermit {
								val request = requestQueue.removeFirstOrNull()
								request?.complete(answer) ?: error("No request to answer")
							}
						}
					}
				}
				debug("The socket has been closed.")
				scope.cancel("The server socket has been closed.")
			} catch (e: SocketException) {
				debug("The socket has been closed externally.")
				scope.cancel("The socket has been closed while the client was trying to read.")
			}
		}
	}

	/**
	 * Asynchronously sends a [message].
	 * This method returns immediately a [Deferred] which will eventually contain the server's response.
	 *
	 * Can be used to send multiple messages concurrently:
	 * ```
	 * val message1 = client.sendAsync(…)
	 * val message2 = client.sendAsync(…)
	 * val response1 = message1.await()
	 * val response2 = message2.await()
	 * ```
	 *
	 * To synchronously send a message, see [send]. If you don't need a reply, see [sendAndForget].
	 */
	suspend fun sendAsync(message: String): Deferred<String> = withContext(Dispatchers.IO) {
		check(!job.isCancelled) { "This client has already died, cannot send a message." }

		val answer = CompletableDeferred<String>(job)
		requestQueueLock.withPermit {
			info("> $message")
			socketSendStream.println(message)
			requestQueue.addLast(answer)
		}
		answer
	}

	/**
	 * Sends a [message] to the server, without waiting for a response.
	 *
	 * Use this method for fire-and-forget commands.
	 *
	 * For commands that expect a response, see [send] and [sendAsync].
	 */
	suspend fun sendAndForget(message: String) = withContext(Dispatchers.IO) {
		requestQueueLock.withPermit {
			info("> $message")
			socketSendStream.println(message)
		}
	}

	/**
	 * Send a [message] to the server, and return the server's response.
	 * This method will suspend until the server has responded.
	 *
	 * See [sendAsync] if you need a [Deferred], and [sendAndForget] if you don't need a reply at all.
	 */
	suspend fun send(message: String) = sendAsync(message).await()

	/**
	 * Disconnects the client, and stops all currently executing commands.
	 * Suspends until the client is completely stopped.
	 */
	suspend fun stop(reason: String = "The 'stop' function was called") {
		info("Disconnecting from the server…")

		@Suppress("BlockingMethodInNonBlockingContext")
		withContext(Dispatchers.IO) {
			socket.close()
		}

		debug("Cancelling all ongoing requests…")
		job.cancel(reason)
		job.join()
		info("Client completely stopped.")
	}

	suspend fun keepAlive() {
		try {
			while (coroutineContext.isActive) {
				val pingValue = Random.nextInt(0, 10000)
				delay(Configuration.displayTimeoutValue * 1000L)
				if (send("ping $pingValue") != "pong $pingValue") {
					debug("Server did not respond correctly to ping")
				}
			}
		} catch (e: CancellationException) {
			info("Keep alive has been terminated")
		}
	}

	fun authenticate(id: String?) = Authentication(id, this, Job(job))
}
