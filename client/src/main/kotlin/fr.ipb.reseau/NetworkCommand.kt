package fr.ipb.reseau

fun okCheck(ret: String): Boolean {
	return ret.split(' ')[0] == "OK"
}

/**
 * Removes the [fish]
 * @returns 'true' if it succeeded and 'false' otherwise
 */
suspend fun Client.removeFish(fish: Fish): String {
	return send("delFish ${fish.name}")
}

/**
 * Adds the [fish]
 * @returns 'true' if it succeeded and 'false' otherwise
 */
suspend fun Client.addFish(fish: Fish): String {
	return send("addFish ${fish.name} at ${fish.currentPosition.start.x}x${fish.currentPosition.start.y},${fish.currentPosition.size.x}x${fish.currentPosition.size.y}, ${fish.behavior}")
}

/**
 * Starts the [fish]
 * @return 'true' if it succeeded and 'false' otherwise
 */
suspend fun Client.startFish(fish: Fish): String {
	return send("startFish ${fish.name}")
}

suspend fun Client.getFishes() {
	sendAndForget("getFishes")
}

fun handleFishUpdate(infos: String, context: Context) {
	require(infos.startsWith("list")) { "The update should start with 'list': '$infos'" }

	val (view, client) = context

	// A line looks like:
	// list [PoissonRouge at 92x40,10x4,5] [PoissonClown at 22x80,12x6,5]

	val fishInfos = infos.split("[")
		.asSequence()
		.drop(1) // We don't care about 'list '
		.map { it.trim().removeSuffix("]") }

	// We now have a single fish information, that looks like this:
	// PoissonRouge at 92x40,10x4,5

	val allFishes = fishInfos.map { fishInfo ->
		val (name, at, pos) = fishInfo.split(" ", limit = 3)

		check(at == "at") { "The second word given for each fish should be 'at', found: '$at'" }

		val (topLeft, size, deadline) = pos.split(",", limit = 3)
		val (x, y) = topLeft.split("x")
		val (width, height) = size.split("x")

		val position = Position(Point(x.toInt(), y.toInt()), Point(width.toInt(), height.toInt()))

		Fish(name, null, position, null)
	}

	view.fishes.clear()
	view.fishes.addAll(allFishes.toList())
}
