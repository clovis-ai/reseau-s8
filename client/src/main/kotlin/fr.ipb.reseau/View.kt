package fr.ipb.reseau

/**
 * A View representing a part of the aquarium
 * @property fishes The list of Fish contained by the View
 * @property position The 2D rectangle of the View
 */
class View(val fishes: MutableList<Fish>, val position: Position)
