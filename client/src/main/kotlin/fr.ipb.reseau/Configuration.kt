package fr.ipb.reseau

import fr.ipb.reseau.Resource.Companion.resource

object Configuration {
	val controllerAddress: String
	val viewId: String
	val controllerPort: Int
	val displayTimeoutValue: Int
	val resources: String
	val logs: String

	init {
		val file = resource("affichage.cfg").reader()
		val lines = file.readLines()

		val configs = HashMap<String, String>()
		for (line in lines) {
			if (!line.startsWith('#') && line.isNotBlank()) {
				val (key, value) = line.split('=', limit = 2).map { it.trim() }
				if (configs.containsKey(key)) {
					throw ConfigurationException("Key appears multiple times: $key")
				} else {
					configs[key] = value
				}
			}
		}
		controllerAddress = assign(configs, "controller-address")
		viewId = assign(configs, "id")
		controllerPort = assign(configs, "controller-port").toInt()
		displayTimeoutValue = assign(configs, "display-timeout-value").toInt()
		resources = assign(configs, "resources")
		logs = assign(configs, "log-location")
	}

	private fun assign(configs: HashMap<String, String>, param: String) =
		configs[param] ?: throw ConfigurationException("Key not found: $param")
}

class ConfigurationException(message: String, cause: Throwable? = null) :
	RuntimeException(message, cause)
