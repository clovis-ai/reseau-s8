# Réseau S8

Projet de réseau S8

## Development

You will need to have installed on your machine:

- The Java Development Kit (JDK)
- A C compiler suite (gcc, libc, make, cmake)
- LuaLaTeX (to compile the report)
- You will need to execute the command `git submodule update --init`, or clone with `--recursive` (otherwise LaTeX compilation will fail)

This project is orchestrated by Gradle and CMake. It is normal that the first run takes some time to startup.

##### The 'install' directory

Use `./gradlew install` to create the `install/` directory with the server, the client, their configuration files, and the reports.

If you do not have a valid LuaLaTeX installation, you can use the command `./gradlew server:install client:install` to create the binaries, their configuration files, but not the reports.

Inside the `install/` directory, you will find:

- `server`, the server binary (run with `./server`)
- `client.jar`, the client binary (run with `java -jar client.jar`)
- `controller.cfg`, the server's configuration file
- `affichage.cfg`, the client's configuration file
- `fishes/`, the fish pictures used by the client

##### Development version

To open in an IDE:

- IntelliJ: open the project root directory, then click the 'load Gradle project' notification.
- CLion: open the project root directory, then [set the project root to server/](https://www.jetbrains.com/help/clion/change-project-root-directory.html), then reload the CMake project.

You can also run everything directly from Gradle:

- `./gradlew server:run` Run the server (the command-line interface has display issues, not recommended)
- `./gradlew client:run` Run the client
- `./gradlew server:test` Run the server's unit tests
- `./gradlew client:test` Run the client's unit tests
- `./gradlew tasks --all` Display the full list of Gradle commands that can be used

Code conventions:

- [General coding style](https://gitlab.com/braindot/legal/-/blob/master/coding-style/STYLE.md)
- [C coding style](https://gitlab.com/braindot/legal/-/blob/master/coding-style/STYLE_C.md)
- [Kotlin coding style](https://gitlab.com/braindot/legal/-/blob/master/coding-style/STYLE_Kt.md)
- [Git conventions](https://gitlab.com/braindot/legal/-/blob/master/coding-style/STYLE_Git.md)

# Authors

The maintainers of this project are :
- Andricque Maëlle (mandricque@enseirb-matmeca.fr)
- Ben Salah Mehdi (mbensalah@enseirb-matmeca.fr)
- Canet Ivan (icanet@enseirb-matmeca.fr)
- Geynet Raphaël (rgeynet@enseirb-matmeca.fr)
- Gond David (dgond@enseirb-matmeca.fr)
